package com.germancarsms.springmvc.util;

import javax.servlet.http.HttpServletRequest;

import com.germancarsms.springmvc.model.CartInfo;

public class Utils {

    public static CartInfo getCartInSession(HttpServletRequest request) {
	CartInfo cartInfo = (CartInfo) request.getSession().getAttribute("myCart");
	if (cartInfo == null) {
	    cartInfo = new CartInfo();
	    request.getSession().setAttribute("myCart", cartInfo);
	}
	return cartInfo;
    }

    public static void removeCartInSession(HttpServletRequest request) {
	request.getSession().removeAttribute("myCart");
    }

    public static void storeLastOrderFromCartInSession(HttpServletRequest request, CartInfo cartInfo) {
	request.getSession().setAttribute("lastOrderedCart", cartInfo);
    }

    public static CartInfo getLastOrderFromCartInSession(HttpServletRequest request) {
	return (CartInfo) request.getSession().getAttribute("lastOrderedCart");
    }

}