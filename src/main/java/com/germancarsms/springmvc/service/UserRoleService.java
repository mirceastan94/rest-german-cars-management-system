package com.germancarsms.springmvc.service;

import java.util.List;

import com.germancarsms.springmvc.entity.Role;

public interface UserRoleService {

    Role findById(int id);

    Role findByType(String type);

    List<Role> findAll();

}
