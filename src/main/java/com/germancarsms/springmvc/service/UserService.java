package com.germancarsms.springmvc.service;

import java.util.List;

import com.germancarsms.springmvc.entity.User;

public interface UserService {

    User findById(int id);

    User findBySSO(String sso);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserBySSO(String sso, String loggedUserName);

    List<User> findAllUsers();

    String isUserValid(Integer id, String sso, String eMail);

}