package com.germancarsms.springmvc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.germancarsms.springmvc.dao.UserDao;
import com.germancarsms.springmvc.entity.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao dao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findById(int id) {
	return dao.findById(id);
    }

    public User findBySSO(String sso) {
	User user = dao.findBySSO(sso);
	return user;
    }

    public void saveUser(User user) {
	user.setPassword(passwordEncoder.encode(user.getPassword()));
	dao.save(user);
    }

    public void updateUser(User user) {
	User entity = dao.findById(user.getId());
	if (entity != null) {
	    entity.setSsoId(user.getSsoId());
	    if (!user.getPassword().equals(entity.getPassword())) {
		entity.setPassword(passwordEncoder.encode(user.getPassword()));
	    }
	    entity.setFirstName(user.getFirstName());
	    entity.setLastName(user.getLastName());
	    entity.setEmail(user.getEmail());
	    entity.setCountry(user.getCountry());
	    entity.setUserRoles(user.getUserRoles());
	}
    }

    public void deleteUserBySSO(String sso, String loggedUserName) {
	dao.deleteBySSO(sso, loggedUserName);
    }

    public List<User> findAllUsers() {
	return dao.findAllUsers();
    }

    public String isUserValid(Integer id, String sso, String eMail) {
	User user = findBySSO(sso);
	String statusCode = null;
	if (!(eMail.contains("@gmail.com") || eMail.contains("@yahoo.com")))
	    statusCode = "Invalid eMail";
	else if (!(user == null || ((id != null) && (user.getId() == id))))
	    statusCode = "Invalid ssoId";
	else {
	    List<User> userList = findAllUsers();
	    for (User currentUser : userList)
		if (currentUser == null || (eMail != null) && (currentUser.getEmail().equals(eMail))) {
		    statusCode = "Dupplicate eMail";
		    break;
		} else
		    statusCode = "Valid User";
	}
	return statusCode;
    }

}
