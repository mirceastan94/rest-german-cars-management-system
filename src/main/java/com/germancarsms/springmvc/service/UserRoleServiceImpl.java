package com.germancarsms.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.germancarsms.springmvc.dao.UserRoleDao;
import com.germancarsms.springmvc.entity.Role;

@Service("userRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    UserRoleDao dao;

    public Role findById(int id) {
	return dao.findById(id);
    }

    public Role findByType(String type) {
	return dao.findByType(type);
    }

    public List<Role> findAll() {
	return dao.findAll();
    }
}
