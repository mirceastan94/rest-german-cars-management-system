package com.germancarsms.springmvc.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;

public class PaginationResult<E> {

    private int totalRecords;
    private int currentPage;
    private List<E> list;
    private int maxResult;
    private int totalPages;

    private int maxNavigationPage;

    private List<Integer> navigationPages;

    @SuppressWarnings("unchecked")
    public PaginationResult(Query query, int page, int maxResult, int maxNavigationPage) {
	final int pageIndex = page - 1 < 0 ? 0 : page - 1;

	int fromRecordIndex = pageIndex * maxResult;
	int maxRecordIndex = fromRecordIndex + maxResult;

	ScrollableResults scrollResults = query.scroll(ScrollMode.SCROLL_INSENSITIVE);

	List<E> resultsList = new ArrayList<E>();

	boolean hasResult = scrollResults.first();

	if (hasResult) {
	    hasResult = scrollResults.scroll(fromRecordIndex);

	    if (hasResult) {
		do {
		    E record = (E) scrollResults.get(0);
		    resultsList.add(record);
		} while (scrollResults.next() && scrollResults.getRowNumber() >= fromRecordIndex
			&& scrollResults.getRowNumber() < maxRecordIndex);

	    }
	    scrollResults.last();
	}
	this.totalRecords = scrollResults.getRowNumber() + 1;
	this.currentPage = pageIndex + 1;
	this.list = resultsList;
	this.maxResult = maxResult;

	this.totalPages = (this.totalRecords / this.maxResult) + 1;
	this.maxNavigationPage = maxNavigationPage;

	if (maxNavigationPage < totalPages) {
	    this.maxNavigationPage = maxNavigationPage;
	}

	this.calcNavigationPages();
    }

    private void calcNavigationPages() {

	this.navigationPages = new ArrayList<Integer>();

	int current = this.currentPage > this.totalPages ? this.totalPages : this.currentPage;

	int begin = current - this.maxNavigationPage / 2;
	int end = current + this.maxNavigationPage / 2;

	navigationPages.add(1);
	if (begin > 2) {
	    navigationPages.add(-1);
	}

	for (int i = begin; i < end; i++) {
	    if (i > 1 && i < this.totalPages) {
		navigationPages.add(i);
	    }
	}

	if (end < this.totalPages - 2) {
	    navigationPages.add(-1);
	}
	navigationPages.add(this.totalPages);
    }

    public int getTotalPages() {
	return totalPages;
    }

    public int getTotalRecords() {
	return totalRecords;
    }

    public int getCurrentPage() {
	return currentPage;
    }

    public List<E> getList() {
	return list;
    }

    public int getMaxResult() {
	return maxResult;
    }

    public List<Integer> getNavigationPages() {
	return navigationPages;
    }

}