package com.germancarsms.springmvc.model;

public class OrderDetailInfo {
    private String id;

    private String productCode;
    private String productName;
    private int quantity;
    private String price;
    private double amount;
    private String type;
    private String mileage;

    public OrderDetailInfo() {

    }

    // Used for Hibernate Query.
    public OrderDetailInfo(String id, String productCode, //
	    String productName, int quantity, String price, double amount) {
	this.id = id;
	this.productCode = productCode;
	this.productName = productName;
	this.quantity = quantity;
	this.price = price;
	this.amount = amount;
    }

    public OrderDetailInfo(String id, String productCode, String productName, int quantity, String price, double amount,
	    String type, String mileage) {
	this.id = id;
	this.productCode = productCode;
	this.productName = productName;
	this.quantity = quantity;
	this.price = price;
	this.amount = amount;
	this.type = type;
	this.mileage = mileage;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getProductCode() {
	return productCode;
    }

    public void setProductCode(String productCode) {
	this.productCode = productCode;
    }

    public String getProductName() {
	return productName;
    }

    public void setProductName(String productName) {
	this.productName = productName;
    }

    public int getQuantity() {
	return quantity;
    }

    public void setQuantity(int quantity) {
	this.quantity = quantity;
    }

    public String getPrice() {
	return price;
    }

    public void setPrice(String price) {
	this.price = price;
    }

    public double getAmount() {
	return amount;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    public String getType() {
	return type;
    }

    public void setType(String productType) {
	this.type = productType;
    }

    public String getMileage() {
	return mileage;
    }

    public void setMileage(String mileage) {
	this.mileage = mileage;
    }

}