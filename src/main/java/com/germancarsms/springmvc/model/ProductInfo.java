package com.germancarsms.springmvc.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.entity.User;

public class ProductInfo {
    private String code;
    private String name;
    private String price;
    private String type;
    private String generation;
    private String year;
    private String engine;
    private String mileage;
    private String status;
    private String comments;
    private User user;

    private boolean newProduct = false;

    // Upload file
    private CommonsMultipartFile fileDataImage;
    private CommonsMultipartFile fileCertificateDocument;
    private CommonsMultipartFile fileIdentityDocument;

    public ProductInfo() {
    }

    public ProductInfo(Product product) {
	this.code = product.getCode();
	this.name = product.getName();
	this.price = product.getPrice();
	this.type = product.getType();
	this.generation = product.getGeneration();
	this.year = product.getYear();
	this.engine = product.getEngine();
	this.mileage = product.getMileage();
	this.status = product.getStatus();
	this.comments = product.getComments();
	this.user = product.getUser();
    }

    // Used for Hibernate query
    public ProductInfo(String code, String name, String price, String type, String generation, String year,
	    String engine, String mileage, String status, String comments, User user) {
	this.code = code;
	this.name = name;
	this.price = price;
	this.type = type;
	this.generation = generation;
	this.year = year;
	this.engine = engine;
	this.mileage = mileage;
	this.status = status;
	this.comments = comments;
	this.user = user;
    }

    // Used for Hibernate query
    public ProductInfo(String code, String name, String price, String type, String generation, String year,
	    String engine, String mileage, String status, String comments) {
	this.code = code;
	this.name = name;
	this.price = price;
	this.type = type;
	this.generation = generation;
	this.year = year;
	this.engine = engine;
	this.mileage = mileage;
	this.status = status;
	this.comments = comments;
    }

    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getMileage() {
	return mileage;
    }

    public void setMileage(String mileage) {
	this.mileage = mileage;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPrice() {
	return price;
    }

    public void setPrice(String price) {
	this.price = price;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getGeneration() {
	return generation;
    }

    public void setGeneration(String generation) {
	this.generation = generation;
    }

    public String getYear() {
	return year;
    }

    public void setYear(String year) {
	this.year = year;
    }

    public String getEngine() {
	return engine;
    }

    public void setEngine(String engine) {
	this.engine = engine;
    }

    public CommonsMultipartFile getFileDataImage() {
	return fileDataImage;
    }

    public void setFileDataImage(CommonsMultipartFile fileDataImage) {
	this.fileDataImage = fileDataImage;
    }

    public CommonsMultipartFile getFileCertificateDocument() {
	return fileCertificateDocument;
    }

    public void setFileCertificateDocument(CommonsMultipartFile fileCertificateDocument) {
	this.fileCertificateDocument = fileCertificateDocument;
    }

    public CommonsMultipartFile getFileIdentityDocument() {
	return fileIdentityDocument;
    }

    public void setFileIdentityDocument(CommonsMultipartFile fileIdentityDocument) {
	this.fileIdentityDocument = fileIdentityDocument;
    }

    public boolean isNewProduct() {
	return newProduct;
    }

    public void setNewProduct(boolean newProduct) {
	this.newProduct = newProduct;
    }

}