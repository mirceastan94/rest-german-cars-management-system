package com.germancarsms.springmvc.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.germancarsms.springmvc.converter.RoleToUserProfileConverter;
import com.germancarsms.springmvc.dao.OrderDAO;
import com.germancarsms.springmvc.dao.ProductDAO;
import com.germancarsms.springmvc.dao.impl.OrderDAOImpl;
import com.germancarsms.springmvc.dao.impl.ProductDAOImpl;

@Configuration
@ComponentScan("com.germancarsms.springmvc.*")
@EnableWebMvc
@EnableTransactionManagement
@PropertySource("classpath:ds-hibernate-cfg.properties")
public class ApplicationContextConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment env;

    @Autowired
    RoleToUserProfileConverter roleToUserProfileConverter;

    @Bean
    public MessageSource messageSource() {
	ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	messageSource.setBasename("messages/validation");
	return messageSource;
    }

    @Bean(name = "viewResolver")
    public InternalResourceViewResolver getViewResolver() {
	InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	viewResolver.setPrefix("/WEB-INF/pages/");
	viewResolver.setSuffix(".jsp");
	return viewResolver;
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
	CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();

	return commonsMultipartResolver;
    }

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
	DriverManagerDataSource dataSource = new DriverManagerDataSource();

	dataSource.setDriverClassName(env.getProperty("ds.database-driver"));
	dataSource.setUrl(env.getProperty("ds.url"));
	dataSource.setUsername(env.getProperty("ds.username"));
	dataSource.setPassword(env.getProperty("ds.password"));

	return dataSource;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
	Properties properties = new Properties();

	properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
	properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
	properties.put("current_session_context_class", env.getProperty("current_session_context_class"));

	LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

	factoryBean.setPackagesToScan(new String[] { "com.germancarsms.springmvc.entity" });
	factoryBean.setDataSource(dataSource);
	factoryBean.setHibernateProperties(properties);
	factoryBean.afterPropertiesSet();

	SessionFactory sf = factoryBean.getObject();
	return sf;
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
	HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);

	return transactionManager;
    }

    /**
     * Configure Converter to be used. In this instance, the converter is needed to
     * convert string values[Roles] to UserRoles in registration.jsp
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
	registry.addConverter(roleToUserProfileConverter);
    }

    @Bean(name = "productDAO")
    public ProductDAO getProductDAO() {
	return new ProductDAOImpl();
    }

    @Bean(name = "orderDAO")
    public OrderDAO getOrderDAO() {
	return new OrderDAOImpl();
    }

}
