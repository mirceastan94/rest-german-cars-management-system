package com.germancarsms.springmvc.dao;

import java.util.List;

import com.germancarsms.springmvc.entity.User;

public interface UserDao {

	User findById(int id);

	User findBySSO(String sso);

	void save(User user);

	void deleteBySSO(String sso, String loggedUserName);

	List<User> findAllUsers();

}
