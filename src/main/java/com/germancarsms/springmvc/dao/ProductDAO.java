package com.germancarsms.springmvc.dao;

import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.entity.User;
import com.germancarsms.springmvc.model.PaginationResult;
import com.germancarsms.springmvc.model.ProductInfo;

public interface ProductDAO {

    public Product findProduct(String code);

    public ProductInfo findProductInfo(String code);

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage);

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String filter);

    public PaginationResult<ProductInfo> queryUserProducts(int page, int maxResult, int maxNavigationPage,
	    String filter, User userID);

    public PaginationResult<ProductInfo> queryAdminProducts(int page, int maxResult, int maxNavigationPage,
	    User userID);

    public void save(ProductInfo productInfo, User user);

    public void update(ProductInfo productInfo, String filter);

    public void delete(ProductInfo productInfo);

}
