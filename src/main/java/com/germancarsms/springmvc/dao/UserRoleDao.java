package com.germancarsms.springmvc.dao;

import java.util.List;

import com.germancarsms.springmvc.entity.Role;

public interface UserRoleDao {

	List<Role> findAll();

	Role findByType(String type);

	Role findById(int id);
}
