package com.germancarsms.springmvc.dao;

import java.util.LinkedHashMap;
import java.util.List;

import com.germancarsms.springmvc.model.CartInfo;
import com.germancarsms.springmvc.model.OrderDetailInfo;
import com.germancarsms.springmvc.model.OrderInfo;
import com.germancarsms.springmvc.model.PaginationResult;

public interface OrderDAO {

    public void saveOrder(CartInfo cartInfo);

    public PaginationResult<OrderInfo> listOrderInfoTotal(int page, int maxResult, int maxNavigationPage);

    public PaginationResult<OrderInfo> listOrderInfoCheap(int page, int maxResult, int maxNavigationPage);

    public PaginationResult<OrderInfo> listOrderInfoRomanian(int page, int maxResult, int maxNavigationPage);

    public PaginationResult<OrderInfo> listOrderInfoLuxury(int page, int maxResult, int maxNavigationPage);

    public OrderInfo getOrderInfo(String orderId);

    public List<OrderDetailInfo> listOrderDetailInfos(String orderId);

    public LinkedHashMap<String, Integer> querySoldProductsPeriod();

    public LinkedHashMap<String, Integer> querySoldProductsBrand();

    public LinkedHashMap<String, Integer> querySoldProductsPrice();

    public LinkedHashMap<String, Integer> querySoldProductsType();

    public LinkedHashMap<String, Integer> querySoldProductsMileage();
}