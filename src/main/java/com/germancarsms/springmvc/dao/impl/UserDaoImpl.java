package com.germancarsms.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.germancarsms.springmvc.dao.AbstractDao;
import com.germancarsms.springmvc.dao.UserDao;
import com.germancarsms.springmvc.entity.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    public User findById(int id) {
	User user = getByKey(id);
	if (user != null) {
	    Hibernate.initialize(user.getUserRoles());
	}
	return user;
    }

    public User findBySSO(String sso) {
	Criteria crit = createEntityCriteria();
	crit.add(Restrictions.eq("ssoId", sso));
	User user = (User) crit.uniqueResult();
	if (user != null) {
	    Hibernate.initialize(user.getUserRoles());
	}
	return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
	Criteria criteria = createEntityCriteria().addOrder(Order.asc("firstName"));
	criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	List<User> users = (List<User>) criteria.list();

	return users;
    }

    public void save(User user) {
	persist(user);
    }

    public void deleteBySSO(String sso, String loggedUserName) {
	Criteria crit = createEntityCriteria();
	crit.add(Restrictions.eq("ssoId", sso));
	User user = (User) crit.uniqueResult();
	if (!user.getSsoId().equals(loggedUserName))
	    delete(user);
    }

}
