package com.germancarsms.springmvc.dao.impl;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.germancarsms.springmvc.dao.OrderDAO;
import com.germancarsms.springmvc.dao.ProductDAO;
import com.germancarsms.springmvc.entity.Order;
import com.germancarsms.springmvc.entity.OrderDetail;
import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.model.CartInfo;
import com.germancarsms.springmvc.model.CartLineInfo;
import com.germancarsms.springmvc.model.CustomerInfo;
import com.germancarsms.springmvc.model.OrderDetailInfo;
import com.germancarsms.springmvc.model.OrderInfo;
import com.germancarsms.springmvc.model.PaginationResult;

@Transactional
public class OrderDAOImpl implements OrderDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ProductDAO productDAO;

    private int getMaxOrderNum() {
	String sql = "Select max(o.orderNum) from " + Order.class.getName() + " o ";
	Session session = sessionFactory.getCurrentSession();
	Query query = session.createQuery(sql);
	Integer value = (Integer) query.uniqueResult();
	if (value == null) {
	    return 0;
	}
	return value;
    }

    @Override
    public void saveOrder(CartInfo cartInfo) {
	Session session = sessionFactory.getCurrentSession();

	int orderNum = this.getMaxOrderNum() + 1;
	Order order = new Order();

	order.setId(UUID.randomUUID().toString());
	order.setOrderNum(orderNum);
	order.setOrderDate(new Date());
	order.setAmount(cartInfo.getAmountTotal());

	CustomerInfo customerInfo = cartInfo.getCustomerInfo();
	order.setCustomerName(customerInfo.getName());
	order.setCustomerEmail(customerInfo.getEmail());
	order.setCustomerPhone(customerInfo.getPhone());
	order.setCustomerAddress(customerInfo.getAddress());
	order.setCustomerCountry(customerInfo.getCountry());

	session.persist(order);

	List<CartLineInfo> lines = cartInfo.getCartLines();

	for (CartLineInfo line : lines) {
	    OrderDetail detail = new OrderDetail();
	    detail.setId(UUID.randomUUID().toString());
	    detail.setOrder(order);
	    detail.setAmount(line.getAmount());
	    detail.setPrice(line.getProductInfo().getPrice());
	    detail.setQuantity(line.getQuantity());
	    detail.setType(line.getProductInfo().getType());
	    detail.setMileage(line.getProductInfo().getMileage());

	    String code = line.getProductInfo().getCode();
	    Product product = this.productDAO.findProduct(code);
	    detail.setProduct(product);

	    session.persist(detail);
	}

	cartInfo.setOrderNum(orderNum);
    }

    @Override
    public PaginationResult<OrderInfo> listOrderInfoTotal(int page, int maxResult, int maxNavigationPage) {
	String sql = "Select new " + OrderInfo.class.getName() + "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
		+ " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone, ord.customerCountry) "
		+ " from " + Order.class.getName() + " ord " + " order by ord.orderNum desc";
	Session session = this.sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);

	return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<OrderInfo> listOrderInfoCheap(int page, int maxResult, int maxNavigationPage) {
	String sql = "Select new " + OrderInfo.class.getName() + "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
		+ " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone, ord.customerCountry) "
		+ " from " + Order.class.getName() + " ord " + " where ord.amount < 15000 ";
	Session session = this.sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);

	return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<OrderInfo> listOrderInfoRomanian(int page, int maxResult, int maxNavigationPage) {
	String sql = "Select new " + OrderInfo.class.getName() + "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
		+ " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone, ord.customerCountry) "
		+ " from " + Order.class.getName() + " ord " + " where ord.customerCountry = 'Romania' ";
	Session session = this.sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);

	return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<OrderInfo> listOrderInfoLuxury(int page, int maxResult, int maxNavigationPage) {
	String sql = "Select new " + OrderInfo.class.getName() + "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
		+ " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone, ord.customerCountry) "
		+ " from " + Order.class.getName() + " ord " + " where ord.amount > 65000";
	Session session = this.sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);

	return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
    }

    @SuppressWarnings("unchecked")
    @Override
    public LinkedHashMap<String, Integer> querySoldProductsPeriod() {
	LinkedHashMap<String, Integer> soldProducts = new LinkedHashMap<String, Integer>();
	final String todayQuery = "Select new " + OrderInfo.class.getName() + "(ord.id) from " + Order.class.getName()
		+ " ord " + " where trunc(ord.orderDate) = trunc(sysdate)";
	final String lastDaysQuery = "Select new " + OrderInfo.class.getName() + "(ord.id) from "
		+ Order.class.getName() + " ord "
		+ " where ord.orderDate between TRUNC(sysdate - 7) AND TRUNC(sysdate+1)";
	final String lastMonthQuery = "Select new " + OrderInfo.class.getName() + "(ord.id) from "
		+ Order.class.getName() + " ord "
		+ " where ord.orderDate between add_months(trunc(sysdate, 'MM'), -1) and trunc(sysdate+1)";
	final String lastMonthsQuery = "Select new " + OrderInfo.class.getName() + "(ord.id) from "
		+ Order.class.getName() + " ord "
		+ " where ord.orderDate between add_months(trunc(sysdate, 'MM'), -3) and trunc(sysdate+1)";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(todayQuery);
	List<OrderDetailInfo> results = query.list();
	soldProducts.put("Today", results.size());
	results.clear();

	query = session.createQuery(lastDaysQuery);
	results = query.list();
	soldProducts.put("Last 7 days", results.size());
	results.clear();

	query = session.createQuery(lastMonthQuery);
	results = query.list();
	soldProducts.put("Last month", results.size());
	results.clear();

	query = session.createQuery(lastMonthsQuery);
	results = query.list();
	soldProducts.put("Last 6 months", results.size());
	results.clear();

	return soldProducts;
    }

    @SuppressWarnings("unchecked")
    @Override
    public LinkedHashMap<String, Integer> querySoldProductsBrand() {
	LinkedHashMap<String, Integer> soldProducts = new LinkedHashMap<String, Integer>();

	final String porscheQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :porsche ";
	final String mercedesQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :mercedes ";
	final String bmwQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :bmw ";
	final String audiQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :audi ";
	final String volksWagenQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :volkswagen ";
	final String opelQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.product.name like :opel ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(porscheQuery);
	query.setParameter("porsche", "%Porsche%");
	List<OrderDetailInfo> results = query.list();
	soldProducts.put("Porsche", results.size());
	results.clear();

	query = session.createQuery(mercedesQuery);
	query.setParameter("mercedes", "%Mercedes%");
	results = query.list();
	soldProducts.put("Mercedes", results.size());
	results.clear();

	query = session.createQuery(bmwQuery);
	query.setParameter("bmw", "%BMW%");
	results = query.list();
	soldProducts.put("BMW", results.size());
	results.clear();

	query = session.createQuery(audiQuery);
	query.setParameter("audi", "%Audi%");
	results = query.list();
	soldProducts.put("Audi", results.size());
	results.clear();

	query = session.createQuery(volksWagenQuery);
	query.setParameter("volkswagen", "%Volkswagen%");
	results = query.list();
	soldProducts.put("Volkswagen", results.size());
	results.clear();

	query = session.createQuery(opelQuery);
	query.setParameter("opel", "%Opel%");
	results = query.list();
	soldProducts.put("Opel", results.size());
	results.clear();

	return soldProducts;

    }

    @Override
    public LinkedHashMap<String, Integer> querySoldProductsPrice() {

	LinkedHashMap<String, Integer> soldProducts = new LinkedHashMap<String, Integer>();

	final String veryCheapQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount < 5000";
	final String cheapQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount < 15000 and d.amount > 4999";
	final String averageAmountQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount < 30000 and d.amount > 14999 ";
	final String expensiveQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount < 45000 and d.amount > 29999 ";
	final String veryExpensiveQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount < 65000 and d.amount > 44999";
	final String luxuryQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.amount > 64999 ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(veryCheapQuery);
	List<OrderDetailInfo> results = query.list();
	soldProducts.put("Very cheap (up to 5000$)", results.size());
	results.clear();

	query = session.createQuery(cheapQuery);
	results = query.list();
	soldProducts.put("Cheap (5000$ - 14999$)", results.size());
	results.clear();

	query = session.createQuery(averageAmountQuery);
	results = query.list();
	soldProducts.put("Average (15000$ - 29999$)", results.size());
	results.clear();

	query = session.createQuery(expensiveQuery);
	results = query.list();
	soldProducts.put("Expensive (30000$ - 44999$)", results.size());
	results.clear();

	query = session.createQuery(veryExpensiveQuery);
	results = query.list();
	soldProducts.put("Very expensive (45000$ - 64999$)", results.size());
	results.clear();

	query = session.createQuery(luxuryQuery);
	results = query.list();
	soldProducts.put("Luxury (above 65000$)", results.size());
	results.clear();

	return soldProducts;
    }

    @Override
    public LinkedHashMap<String, Integer> querySoldProductsType() {

	LinkedHashMap<String, Integer> soldProducts = new LinkedHashMap<String, Integer>();

	String cabrioQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :cabrio ";
	String sedanQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :sedan ";
	String coupeQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :coupe ";
	String hatchBackQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :hatchBack ";
	String breakQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :break ";
	String suvQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.type = :suv ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(cabrioQuery);
	query.setParameter("cabrio", "Cabrio");
	List<OrderDetailInfo> results = query.list();
	soldProducts.put("Cabrio", results.size());
	results.clear();

	query = session.createQuery(sedanQuery);
	query.setParameter("sedan", "Sedan");
	results = query.list();
	soldProducts.put("Sedan", results.size());
	results.clear();

	query = session.createQuery(coupeQuery);
	query.setParameter("coupe", "Coupe");
	results = query.list();
	soldProducts.put("Coupe", results.size());
	results.clear();

	query = session.createQuery(hatchBackQuery);
	query.setParameter("hatchBack", "Hatchback");
	results = query.list();
	soldProducts.put("Hatchback", results.size());
	results.clear();

	query = session.createQuery(breakQuery);
	query.setParameter("break", "Break");
	results = query.list();
	soldProducts.put("Break", results.size());
	results.clear();

	query = session.createQuery(suvQuery);
	query.setParameter("suv", "SUV");
	results = query.list();
	soldProducts.put("SUV", results.size());
	results.clear();

	return soldProducts;
    }

    @SuppressWarnings("unchecked")
    @Override
    public LinkedHashMap<String, Integer> querySoldProductsMileage() {

	LinkedHashMap<String, Integer> soldProducts = new LinkedHashMap<String, Integer>();

	String almostNewQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage < 10000 ";
	String verySmallUsageQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage < 25000 and d.mileage > 9999";
	String smallUsageQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage < 50000 and d.mileage > 24999";
	String averageUsageQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage < 100000 and d.mileage > 49999";
	String significantUsageQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage < 200000 and d.mileage > 99999 ";
	String hugeUsageQuery = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount, d.type, d.mileage) " + " from "
		+ OrderDetail.class.getName() + " d " + " where d.mileage > 199999 ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(almostNewQuery);
	List<OrderDetailInfo> results = query.list();
	soldProducts.put("Almost new (up to 9999 KM)", results.size());
	results.clear();

	query = session.createQuery(verySmallUsageQuery);
	results = query.list();
	soldProducts.put("Very small usage (10000 KM - 24999 KM)", results.size());
	results.clear();

	query = session.createQuery(smallUsageQuery);
	results = query.list();
	soldProducts.put("Small usage (25000 KM - 49999 KM)", results.size());
	results.clear();

	query = session.createQuery(averageUsageQuery);
	results = query.list();
	soldProducts.put("Average usage (50000 KM - 99999 KM)", results.size());
	results.clear();

	query = session.createQuery(significantUsageQuery);
	results = query.list();
	soldProducts.put("Significant usage (100000 KM - 199999 KM)", results.size());
	results.clear();

	query = session.createQuery(hugeUsageQuery);
	results = query.list();
	soldProducts.put("Huge usage (above 200000 KM)", results.size());
	results.clear();

	return soldProducts;
    }

    public Order findOrder(String orderId) {
	Session session = sessionFactory.getCurrentSession();
	Criteria crit = session.createCriteria(Order.class);
	crit.add(Restrictions.eq("id", orderId));
	return (Order) crit.uniqueResult();
    }

    @Override
    public OrderInfo getOrderInfo(String orderId) {
	Order order = this.findOrder(orderId);
	if (order == null) {
	    return null;
	}
	return new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(), order.getAmount(),
		order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(), order.getCustomerPhone(),
		order.getCustomerCountry());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderDetailInfo> listOrderDetailInfos(String orderId) {
	String sql = "Select new " + OrderDetailInfo.class.getName()
		+ "(d.id, d.product.code, d.product.name , d.quantity,d.price,d.amount,d.product.type,d.product.mileage) "
		+ " from " + OrderDetail.class.getName() + " d "//
		+ " where d.order.id = :orderId ";

	Session session = this.sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);
	query.setParameter("orderId", orderId);

	return query.list();
    }

}