package com.germancarsms.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.germancarsms.springmvc.dao.AbstractDao;
import com.germancarsms.springmvc.dao.UserRoleDao;
import com.germancarsms.springmvc.entity.Role;

@Repository("userRoleDao")
public class UserRoleDaoImpl extends AbstractDao<Integer, Role> implements UserRoleDao {

    public Role findById(int id) {
	return getByKey(id);
    }

    public Role findByType(String type) {
	Criteria crit = createEntityCriteria();
	crit.add(Restrictions.eq("type", type));
	return (Role) crit.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Role> findAll() {
	Criteria crit = createEntityCriteria();
	crit.addOrder(Order.asc("type"));
	return (List<Role>) crit.list();
    }

}
