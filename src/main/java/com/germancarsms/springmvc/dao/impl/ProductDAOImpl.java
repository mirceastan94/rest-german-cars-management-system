package com.germancarsms.springmvc.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.germancarsms.springmvc.dao.ProductDAO;
import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.entity.User;
import com.germancarsms.springmvc.model.PaginationResult;
import com.germancarsms.springmvc.model.ProductInfo;

@Transactional
public class ProductDAOImpl implements ProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Product findProduct(String code) {
	Session session = sessionFactory.getCurrentSession();
	Criteria crit = session.createCriteria(Product.class);
	crit.add(Restrictions.eq("code", code));
	return (Product) crit.uniqueResult();
    }

    @Override
    public ProductInfo findProductInfo(String code) {
	Product product = this.findProduct(code);
	if (product == null) {
	    return null;
	}
	return new ProductInfo(product.getCode(), product.getName(), product.getPrice(), product.getType(),
		product.getGeneration(), product.getYear(), product.getEngine(), product.getMileage(),
		product.getStatus(), product.getComments(), product.getUser());
    }

    @Override
    public void save(ProductInfo productInfo, User loggedInUser) {
	String code = productInfo.getCode();

	Product product = null;

	boolean isNew = false;
	if (code != null) {
	    product = this.findProduct(code);
	}
	if (product == null) {
	    isNew = true;
	    product = new Product();
	    product.setCreateDate(new Date());
	}
	product.setCode(code);
	product.setName(productInfo.getName());
	product.setPrice(productInfo.getPrice());
	product.setType(productInfo.getType());
	product.setGeneration(productInfo.getGeneration());
	product.setYear(productInfo.getYear());
	product.setEngine(productInfo.getEngine());
	product.setMileage(productInfo.getMileage());
	product.setComments(productInfo.getComments());
	product.setStatus("REVIEW");
	product.setUser(loggedInUser);

	if (productInfo.getFileDataImage() != null) {
	    byte[] productImage = productInfo.getFileDataImage().getBytes();
	    if (productImage != null && productImage.length > 0) {
		product.setImage(productImage);
	    }
	}
	if (productInfo.getFileCertificateDocument() != null) {
	    byte[] productCertificateDocument = productInfo.getFileCertificateDocument().getBytes();
	    if (productCertificateDocument != null && productCertificateDocument.length > 0) {
		product.setCertificate(productCertificateDocument);
	    }
	}
	if (productInfo.getFileIdentityDocument() != null) {
	    byte[] productIdentityDocument = productInfo.getFileIdentityDocument().getBytes();
	    if (productIdentityDocument != null && productIdentityDocument.length > 0) {
		product.setIdentity(productIdentityDocument);
	    }
	}
	if (isNew) {
	    this.sessionFactory.getCurrentSession().persist(product);
	}
	// If any error happens within the database, an appropiate Exception
	// will be thrown immediately
	this.sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void update(ProductInfo productInfo, String status) {
	String code = productInfo.getCode();
	Product product = this.findProduct(code);
	product.setComments(productInfo.getComments());
	switch (status) {
	default: {
	    product.setStatus("REVIEW");
	    break;
	}
	case "allow": {
	    product.setStatus("PUBLISHED");
	    break;
	}
	case "rejected": {
	    product.setStatus("REJECTED");
	    break;
	}
	case "sold": {
	    product.setStatus("SOLD");
	    break;
	}
	case "inCart": {
	    product.setStatus("INCART");
	    break;
	}
	}
	this.sessionFactory.getCurrentSession().update(product);

	this.sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void delete(ProductInfo productInfo) {
	String code = productInfo.getCode();
	Product product = null;
	if (code != null) {
	    product = this.findProduct(code);
	    this.sessionFactory.getCurrentSession().delete(product);
	    this.sessionFactory.getCurrentSession().flush();
	}
    }

    @Override
    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String filter) {
	String sql = "Select new " + ProductInfo.class.getName()
		+ "(p.code, p.name, p.price, p.type, p.generation, p.year, p.engine, p.mileage, p.status, p.comments, p.user) "
		+ " from " + Product.class.getName() + " p ";
	switch (filter) {
	case "dateDesc": {
	    sql += " where p.status = 'PUBLISHED' order by p.createDate desc ";
	    break;
	}
	case "nameAsc": {
	    sql += " where p.status = 'PUBLISHED' order by p.name asc ";
	    break;
	}
	case "nameDesc": {
	    sql += " where p.status = 'PUBLISHED' order by p.name desc ";
	    break;
	}
	case "priceAsc": {
	    sql += " where p.status = 'PUBLISHED' order by p.price asc ";
	    break;
	}
	case "priceDesc": {
	    sql += " where p.status = 'PUBLISHED' order by p.price desc ";
	    break;
	}
	case "mileageAsc": {
	    sql += " where p.status = 'PUBLISHED' order by p.mileage asc ";
	    break;
	}
	case "mileageDesc": {
	    sql += " where p.status = 'PUBLISHED' order by p.mileage desc ";
	    break;
	}
	case "pendingList": {
	    sql += " where p.status = 'REVIEW' AND p.user=:loggedInUser: order by p.createDate desc ";
	    break;
	}
	default: {
	    sql += " where p.status = 'PUBLISHED' or p.status = 'REVIEW' or p.status = 'REJECTED' or p.status ='SOLD' order by p.createDate desc ";
	    break;
	}
	}

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);

	return new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<ProductInfo> queryUserProducts(int page, int maxResult, int maxNavigationPage,
	    String filter, User loggedUser) {
	String sql = "Select new " + ProductInfo.class.getName()
		+ "(p.code, p.name, p.price, p.type, p.generation, p.year, p.engine, p.mileage, p.status, p.comments, p.user)"
		+ " from " + Product.class.getName() + " p where p.user = :loggedUser"
		+ " AND (p.status = 'PUBLISHED' or p.status = 'REVIEW' "
		+ "or p.status = 'REJECTED' or p.status ='SOLD') order by p.createDate desc ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);
	query.setParameter("loggedUser", loggedUser);

	return new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
    }

    @Override
    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
	return queryProducts(page, maxResult, maxNavigationPage, null);
    }

    @Override
    public PaginationResult<ProductInfo> queryAdminProducts(int page, int maxResult, int maxNavigationPage,
	    User loggedInUser) {
	String sql = "Select new " + ProductInfo.class.getName()
		+ "(p.code, p.name, p.price, p.type, p.generation, p.year, p.engine, p.mileage, p.status, p.comments, p.user) "
		+ " from " + Product.class.getName() + " p ";
	sql += " where p.status = 'REVIEW' AND p.user<>:loggedInUser order by p.createDate desc ";

	Session session = sessionFactory.getCurrentSession();

	Query query = session.createQuery(sql);
	query.setParameter("loggedInUser", loggedInUser);

	return new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
    }

}