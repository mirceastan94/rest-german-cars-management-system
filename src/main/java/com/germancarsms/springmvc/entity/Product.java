package com.germancarsms.springmvc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Products")
public class Product implements Serializable {

    private static final long serialVersionUID = 201195988147252957L;

    private String code;
    private String name;
    private String price;
    private byte[] image;
    private byte[] certificate;
    private byte[] identity;
    private String type;
    private String generation;
    private String year;
    private String engine;
    private String mileage;
    private String status;
    private String comments;
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false, foreignKey = @ForeignKey(name = "PRODUCT_USER_FK"))
    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    @Column(name = "Comments", length = 255, nullable = false)
    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    @Column(name = "Status", length = 20, nullable = false)
    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    @Lob
    @Column(name = "Identity", length = Integer.MAX_VALUE, nullable = true)
    public byte[] getIdentity() {
	return identity;
    }

    public void setIdentity(byte[] identity) {
	this.identity = identity;
    }

    @Lob
    @Column(name = "Certificate", length = Integer.MAX_VALUE, nullable = true)
    public byte[] getCertificate() {
	return certificate;
    }

    public void setCertificate(byte[] certificate) {
	this.certificate = certificate;
    }

    @Column(name = "Mileage", length = 20, nullable = false)
    public String getMileage() {
	return mileage;
    }

    public void setMileage(String mileage) {
	this.mileage = mileage;
    }

    private Date createDate;

    public Product() {
    }

    @Id
    @Column(name = "Code", length = 20, nullable = false)
    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    @Column(name = "Name", length = 255, nullable = false)
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Column(name = "Price", nullable = false)
    public String getPrice() {
	return price;
    }

    public void setPrice(String price) {
	this.price = price;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Create_Date", nullable = false)
    public Date getCreateDate() {
	return createDate;
    }

    public void setCreateDate(Date createDate) {
	this.createDate = createDate;
    }

    @Lob
    @Column(name = "Image", length = Integer.MAX_VALUE, nullable = true)
    public byte[] getImage() {
	return image;
    }

    public void setImage(byte[] image) {
	this.image = image;
    }

    @Column(name = "Type", length = 20, nullable = false)
    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    @Column(name = "Generation", length = 20, nullable = false)
    public String getGeneration() {
	return generation;
    }

    public void setGeneration(String generation) {
	this.generation = generation;
    }

    @Column(name = "Year", length = 20, nullable = false)
    public String getYear() {
	return year;
    }

    public void setYear(String year) {
	this.year = year;
    }

    @Column(name = "Engine", length = 20, nullable = false)
    public String getEngine() {
	return engine;
    }

    public void setEngine(String engine) {
	this.engine = engine;
    }

}