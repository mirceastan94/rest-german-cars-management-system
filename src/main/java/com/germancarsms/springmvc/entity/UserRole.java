package com.germancarsms.springmvc.entity;

import java.io.Serializable;

public enum UserRole implements Serializable {
    USER("USER"), ADMIN("ADMIN");

    String userRole;

    private UserRole(String userRole) {
	this.userRole = userRole;
    }

    public String getUserRole() {
	return userRole;
    }

}
