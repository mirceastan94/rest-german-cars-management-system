package com.germancarsms.springmvc.entity;

import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1750488343929395331L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_SEQ")
    @SequenceGenerator(name = "USERS_SEQ", sequenceName = "USERS_SEQ", allocationSize = 1)
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Integer userId;

    @NotEmpty
    @Column(name = "SSO_ID", unique = true, nullable = false)
    private String ssoId;

    @NotEmpty
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @NotEmpty
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @NotEmpty
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @NotEmpty
    @Column(name = "EMAIL", nullable = false)
    private String email;

    @NotEmpty
    @Column(name = "COUNTRY", nullable = false)
    private String country;

    @NotEmpty
    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "USER_ROLES", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
	    @JoinColumn(name = "ROLE_ID") })
    private Set<Role> userRoles = new HashSet<Role>();

    public Integer getId() {
	return userId;
    }

    public void setId(Integer id) {
	this.userId = id;
    }

    public String getSsoId() {
	return ssoId;
    }

    public void setSsoId(String ssoId) {
	this.ssoId = ssoId;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public Set<Role> getUserRoles() {
	return userRoles;
    }

    public void setUserRoles(Set<Role> userRoles) {
	this.userRoles = userRoles;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((userId == null) ? 0 : userId.hashCode());
	result = prime * result + ((ssoId == null) ? 0 : ssoId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof User))
	    return false;
	User other = (User) obj;
	if (userId == null) {
	    if (other.userId != null)
		return false;
	} else if (!userId.equals(other.userId))
	    return false;
	if (ssoId == null) {
	    if (other.ssoId != null)
		return false;
	} else if (!ssoId.equals(other.ssoId))
	    return false;
	return true;
    }


    @Override
    public String toString() {
	return "User [id=" + userId + ", ssoId=" + ssoId + ", firstName=" + firstName
		+ ", lastName=" + lastName + ", email=" + email + "]";
    }

}
