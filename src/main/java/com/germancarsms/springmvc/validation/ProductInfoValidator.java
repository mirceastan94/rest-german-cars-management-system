package com.germancarsms.springmvc.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.germancarsms.springmvc.dao.ProductDAO;
import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.model.ProductInfo;

@Component
public class ProductInfoValidator implements Validator {

    @Autowired
    private ProductDAO productDAO;

    @Override
    public boolean supports(Class<?> clazz) {
	return clazz == ProductInfo.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
	ProductInfo productInfo = (ProductInfo) target;

	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "NotEmpty.productForm.code");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.productForm.name");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "NotEmpty.productForm.type");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty.productForm.price");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "generation", "NotEmpty.productForm.generation");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "year", "NotEmpty.productForm.year");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "engine", "NotEmpty.productForm.engine");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mileage", "NotEmpty.productForm.mileage");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "comments", "NotEmpty.productForm.comments");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fileDataImage", "NotEmpty.productForm.fileDataImage");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fileCertificateDocument",
		"NotEmpty.productForm.fileCertificateDocument");
	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fileIdentityDocument",
		"NotEmpty.productForm.fileIdentityDocument");

	final String code = productInfo.getCode();
	if (code != null && code.length() > 0) {
	    if (code.matches("\\s+")) {
		errors.rejectValue("code", "Pattern.productForm.code");
	    } else if (productInfo.isNewProduct()) {
		Product product = productDAO.findProduct(code);
		if (product != null) {
		    errors.rejectValue("code", "Duplicate.productForm.code");
		}
	    }
	}
    }

}
