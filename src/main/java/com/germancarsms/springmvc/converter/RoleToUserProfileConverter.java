package com.germancarsms.springmvc.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.germancarsms.springmvc.entity.Role;
import com.germancarsms.springmvc.service.UserRoleService;

/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class RoleToUserProfileConverter implements Converter<Object, Role> {

	@Autowired
	UserRoleService userRoleService;

	/**
	 * Gets UserRole by Id
	 * 
	 */
	public Role convert(Object element) {
		Integer id = Integer.parseInt((String) element);
		Role profile = userRoleService.findById(id);
		return profile;
	}
}