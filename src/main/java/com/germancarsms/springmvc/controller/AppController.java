package com.germancarsms.springmvc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.germancarsms.springmvc.dao.OrderDAO;
import com.germancarsms.springmvc.dao.ProductDAO;
import com.germancarsms.springmvc.entity.Product;
import com.germancarsms.springmvc.entity.Role;
import com.germancarsms.springmvc.entity.User;
import com.germancarsms.springmvc.model.CartInfo;
import com.germancarsms.springmvc.model.CartLineInfo;
import com.germancarsms.springmvc.model.CustomerInfo;
import com.germancarsms.springmvc.model.OrderDetailInfo;
import com.germancarsms.springmvc.model.OrderInfo;
import com.germancarsms.springmvc.model.PaginationResult;
import com.germancarsms.springmvc.model.ProductInfo;
import com.germancarsms.springmvc.service.UserRoleService;
import com.germancarsms.springmvc.service.UserService;
import com.germancarsms.springmvc.util.Utils;
import com.germancarsms.springmvc.validation.CustomerInfoValidator;
import com.germancarsms.springmvc.validation.ProductInfoValidator;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

    @Autowired
    UserService userService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

    @Autowired
    AuthenticationTrustResolver authenticationTrustResolver;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CustomerInfoValidator customerInfoValidator;

    @Autowired
    private ProductInfoValidator productInfoValidator;

    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
	Object target = dataBinder.getTarget();
	if (target == null) {
	    return;
	} else if (target.getClass() == CustomerInfo.class) {
	    dataBinder.setValidator(customerInfoValidator);
	} else if (target.getClass() == ProductInfo.class) {
	    dataBinder.setValidator(productInfoValidator);
	    dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}
    }

    /**
     * The method contains all the registered users
     */
    @RequestMapping(value = { "/list" }, method = RequestMethod.GET)
    public String listUsers(ModelMap model) {

	List<User> users = userService.findAllUsers();
	model.addAttribute("users", users);
	model.addAttribute("loggedinuser", getPrincipal());
	return "userslist";
    }

    /**
     * The method will provide the environment to add a new user
     */
    @RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
    public String newUser(ModelMap model) {
	User user = new User();
	model.addAttribute("user", user);
	model.addAttribute("edit", false);
	model.addAttribute("loggedinuser", getPrincipal());
	return "registration";
    }

    /**
     * The POST method of the same method, attempting to add a new user in the
     * database
     */
    @RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
    public String saveUser(@Valid User user, BindingResult result, Model model) {

	if (result.hasErrors()) {
	    model.addAttribute("loggedinuser", getPrincipal());
	    return "registration";
	}

	String statusCode = userService.isUserValid(user.getId(), user.getSsoId(), user.getEmail());
	if (statusCode.equals("Invalid ssoId")) {
	    FieldError ssoError = new FieldError("user", "ssoId", messageSource.getMessage("non.unique.ssoId",
		    new String[] { user.getSsoId() }, Locale.getDefault()));
	    result.addError(ssoError);
	    return "registration";
	} else if (statusCode.equals("Invalid eMail")) {
	    FieldError eMailError = new FieldError("user", "email",
		    messageSource.getMessage("invalid.eMail", new String[] { user.getEmail() }, Locale.getDefault()));
	    result.addError(eMailError);
	    return "registration";
	} else if (statusCode.equals("Dupplicate eMail")) {
	    FieldError eMailError = new FieldError("user", "email", messageSource.getMessage("non.unique.eMail",
		    new String[] { user.getEmail() }, Locale.getDefault()));
	    result.addError(eMailError);
	    return "registration";
	} else {
	    userService.saveUser(user);

	    model.addAttribute("success",
		    "User  \"" + user.getFirstName() + " " + user.getLastName() + "\" registered successfully");
	    model.addAttribute("loggedinuser", getPrincipal());
	    return "registrationsuccess";
	}
    }

    /**
     * This method will provide the environment to update an already existing
     * user
     */
    @RequestMapping(value = { "/edit-user-{ssoId}" }, method = RequestMethod.GET)
    public String editUser(@PathVariable String ssoId, Model model) {
	User user = userService.findBySSO(ssoId);
	model.addAttribute("user", user);
	model.addAttribute("edit", true);
	model.addAttribute("loggedinuser", getPrincipal());
	return "registration";
    }

    /**
     * This method will be called when the user edit takes place
     */
    @RequestMapping(value = { "/edit-user-{ssoId}" }, method = RequestMethod.POST)
    public String updateUser(@Valid User user, BindingResult result, Model model, @PathVariable String ssoId) {

	if (result.hasErrors()) {
	    return "registration";
	}

	userService.updateUser(user);

	model.addAttribute("success",
		"User " + user.getFirstName() + " " + user.getLastName() + " updated successfully");
	model.addAttribute("loggedinuser", getPrincipal());
	return "registrationsuccess";
    }

    /**
     * This method will delete a user from the database based on its ssoId
     */
    @RequestMapping(value = { "/delete-user-{ssoId}" }, method = RequestMethod.GET)
    public String deleteUser(@PathVariable String ssoId) {
	String loggedUserName = getPrincipal();
	userService.deleteUserBySSO(ssoId, loggedUserName);
	return "redirect:/list";
    }

    /**
     * This method will provide UserRole list to all views
     */
    @ModelAttribute("roles")
    public List<Role> initializeProfiles() {
	return userRoleService.findAll();
    }

    /**
     * This method handles the redirect to the access denied page.
     */
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(Model model) {
	model.addAttribute("loggedinuser", getPrincipal());
	return "accessDenied";
    }

    /**
     * This method handles login GET requests. If users is already logged-in and
     * tries to goto login page again, will be redirected to list page
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
	return "index";
    }

    /**
     * This method handles logout requests.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	if (auth != null) {
	    persistentTokenBasedRememberMeServices.logout(request, response, auth);
	    SecurityContextHolder.getContext().setAuthentication(null);
	}
	return "redirect:/login?logout";
    }

    /**
     * This method returns the principal of the logged in user
     */
    private String getPrincipal() {
	String userName = null;
	Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	if (principal instanceof UserDetails) {
	    userName = ((UserDetails) principal).getUsername();
	} else {
	    userName = principal.toString();
	}
	return userName;
    }

    /**
     * This method will return the logged in user as a User entity
     */
    private User getLoggedInUser() {

	final UserDetails loggedInUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
		.getPrincipal();
	final String userName = loggedInUserDetails.getUsername();
	final User loggedInUser = userService.findBySSO(userName);
	return loggedInUser;
    }

    /**
     * This method will return various statistics by calling multiple orderDAO
     * methods
     */
    @RequestMapping(value = { "/statistics" }, method = RequestMethod.GET)
    public String getStatistics(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	User loggedInUser = getLoggedInUser();
	int page = 1;
	page = Integer.parseInt(pageStr);
	final int MAX_RESULT = 5;
	final int MAX_NAVIGATION_PAGE = 10;

	PaginationResult<OrderInfo> paginationResult = orderDAO.listOrderInfoTotal(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE);

	PaginationResult<OrderInfo> cheapProductsBought = orderDAO.listOrderInfoCheap(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE);

	PaginationResult<OrderInfo> romanianProductsBought = orderDAO.listOrderInfoRomanian(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE);

	PaginationResult<ProductInfo> totalProductsAvailable = productDAO.queryProducts(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE, "nameAsc");

	PaginationResult<ProductInfo> totalProductsPendingAvailable = productDAO.queryAdminProducts(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE, loggedInUser);

	PaginationResult<OrderInfo> luxuryProductsBought = orderDAO.listOrderInfoLuxury(page, MAX_RESULT,
		MAX_NAVIGATION_PAGE);

	model.addAttribute("paginationResult", paginationResult);
	model.addAttribute("cheapProductsBought", cheapProductsBought);
	model.addAttribute("romanianProductsBought", romanianProductsBought);
	model.addAttribute("totalProductsAvailable", totalProductsAvailable);
	model.addAttribute("totalProductsPendingAvailable", totalProductsPendingAvailable);
	model.addAttribute("luxuryProductsBought", luxuryProductsBought);
	return "statistics";
    }

    /**
     * This method will return the page in which one could see more advanced
     * statistics menu
     */
    @RequestMapping(value = { "/dropDownStats" }, method = RequestMethod.GET)
    public String getAdditionalStatistics(Model model,
	    @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	return "detailedStatistics/additionalStatsMain";
    }

    /**
     * This method returns the page with the sold products based on period
     */
    @RequestMapping(value = { "/soldProductsPeriod" }, method = RequestMethod.GET)
    public String getSoldCarsPeriod(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	LinkedHashMap<String, Integer> soldProductsPeriod = orderDAO.querySoldProductsPeriod();
	model.addAttribute("soldProductsPeriod", soldProductsPeriod);
	return "detailedStatistics/soldProductsPeriodStatistics";
    }

    /**
     * This method returns the page with the sold products based on brand
     */
    @RequestMapping(value = { "/soldProductsBrand" }, method = RequestMethod.GET)
    public String getSoldCarsBrand(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	HashMap<String, Integer> soldProductsBrand = orderDAO.querySoldProductsBrand();
	model.addAttribute("soldProductsBrand", soldProductsBrand);
	return "detailedStatistics/soldProductsBrandStatistics";
    }

    /**
     * This method returns the page with the sold products based on price
     */
    @RequestMapping(value = { "/soldProductsPrice" }, method = RequestMethod.GET)
    public String getSoldCarsPrice(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	HashMap<String, Integer> soldProductsPrice = orderDAO.querySoldProductsPrice();
	model.addAttribute("soldProductsPrice", soldProductsPrice);
	return "detailedStatistics/soldProductsPriceStatistics";
    }

    /**
     * This method returns the page with the sold products based on their type
     */
    @RequestMapping(value = { "/soldProductsType" }, method = RequestMethod.GET)
    public String getSoldCarsType(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	HashMap<String, Integer> soldProductsType = orderDAO.querySoldProductsType();
	model.addAttribute("soldProductsType", soldProductsType);
	return "detailedStatistics/soldProductsTypeStatistics";
    }

    /**
     * This method returns the page with the sold products based on their
     * mileage
     */
    @RequestMapping(value = { "/soldProductsMileage" }, method = RequestMethod.GET)
    public String getSoldCarsMileage(Model model, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
	model.addAttribute("loggedinuser", getPrincipal());
	HashMap<String, Integer> soldProductsMileage = orderDAO.querySoldProductsMileage();
	model.addAttribute("soldProductsMileage", soldProductsMileage);
	return "detailedStatistics/soldProductsMileageStatistics";
    }

    /**
     * This method returns the page with a selected product based on its code
     */
    @RequestMapping(value = { "/product" }, method = RequestMethod.GET)
    public String product(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	ProductInfo productInfo = null;
	if (code != null && code.length() > 0) {
	    productInfo = productDAO.findProductInfo(code);
	}
	if (productInfo == null) {
	    productInfo = new ProductInfo();
	    productInfo.setNewProduct(true);
	}
	model.addAttribute("productForm", productInfo);
	return "product";
    }

    /**
     * This method saves the product from the View form in the database
     */
    @RequestMapping(value = { "/product" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String productSave(Model model, @ModelAttribute("productForm") @Validated ProductInfo productInfo,
	    BindingResult result, final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	if (result.hasErrors()) {
	    return "product";
	}
	try {
	    final UserDetails loggedInUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
		    .getPrincipal();
	    final String userName = loggedInUserDetails.getUsername();
	    final User loggedInUser = userService.findBySSO(userName);
	    productDAO.save(productInfo, loggedInUser);
	} catch (HibernateException e) {
	    String message = e.getMessage();
	    model.addAttribute("message", message);
	    return "product";

	}
	return "redirect:/productOfferFinalize";
    }

    /**
     * This method returns the product page with the existing information in the
     * database
     */
    @RequestMapping(value = { "/productEdit" }, method = RequestMethod.GET)
    public String productEdit(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	ProductInfo productInfo = null;
	if (code != null && code.length() > 0) {
	    productInfo = productDAO.findProductInfo(code);
	}
	if (productInfo == null) {
	    productInfo = new ProductInfo();
	    productInfo.setNewProduct(true);
	}
	model.addAttribute("productForm", productInfo);
	return "product";
    }

    /**
     * This method performs the product information update
     */
    @RequestMapping(value = { "/productEdit" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String productEdit(Model model, @ModelAttribute("productForm") @Validated ProductInfo productInfo,
	    BindingResult result, final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	if (result.hasErrors()) {
	    return "product";
	}
	try {
	    final UserDetails loggedInUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
		    .getPrincipal();
	    final String userName = loggedInUserDetails.getUsername();
	    final User loggedInUser = userService.findBySSO(userName);
	    productDAO.save(productInfo, loggedInUser);
	} catch (HibernateException e) {
	    String message = e.getMessage();
	    model.addAttribute("message", message);
	    return "product";

	}
	return "redirect:/productOfferFinalize";
    }

    /**
     * This method displays the page with information of a product that is being
     * reviewed
     */
    @RequestMapping(value = { "/pendingProduct" }, method = RequestMethod.GET)
    public String pendingProduct(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	ProductInfo productInfo = null;
	if (code != null && code.length() > 0) {
	    productInfo = productDAO.findProductInfo(code);
	}
	if (productInfo == null) {
	    productInfo = new ProductInfo();
	    productInfo.setNewProduct(true);
	}
	model.addAttribute("productForm", productInfo);
	return "pendingProduct";
    }

    /**
     * This method displays the page with all the product information, including
     * its pictures and documents
     */
    @RequestMapping(value = { "/productDetail" }, method = RequestMethod.GET)
    public String productDetail(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	ProductInfo productInfo = null;
	if (code != null && code.length() > 0) {
	    productInfo = productDAO.findProductInfo(code);
	}
	if (productInfo == null) {
	    productInfo = new ProductInfo();
	    productInfo.setNewProduct(true);
	}
	model.addAttribute("productForm", productInfo);
	return "productDetail";
    }

    /**
     * This method updates the product status to 'allow'
     */
    @RequestMapping(value = { "/pendingProduct" }, method = { RequestMethod.POST }, params = "allow")
    @Transactional(propagation = Propagation.NEVER)
    public String productAllow(Model model, @ModelAttribute("productForm") @Validated ProductInfo productInfo,
	    BindingResult result, final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	try {
	    productDAO.update(productInfo, "allow");
	} catch (HibernateException e) {
	    String message = e.getMessage();
	    model.addAttribute("message", message);
	    return "product";
	}
	return "redirect:/pendingProductList";
    }

    /**
     * This method updates the product status to 'reject'
     */
    @RequestMapping(value = { "/pendingProduct" }, method = { RequestMethod.POST }, params = "reject")
    @Transactional(propagation = Propagation.NEVER)
    public String productReject(Model model, @ModelAttribute("productForm") @Validated ProductInfo productInfo,
	    BindingResult result, final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	try {
	    productDAO.update(productInfo, "rejected");
	} catch (HibernateException e) {
	    String message = e.getMessage();
	    model.addAttribute("message", message);
	    return "product";

	}
	return "redirect:/pendingProductList";
    }

    /**
     * This method triggers the finished product offer submission page
     */
    @RequestMapping(value = { "/productOfferFinalize" }, method = RequestMethod.GET)
    public String productOfferFinalize(HttpServletRequest request, Model model) {
	model.addAttribute("loggedinuser", getPrincipal());
	return "productOfferFinalize";
    }

    /**
     * This method creates the product removal environments
     */
    @RequestMapping(value = { "/productRemove" }, method = RequestMethod.GET)
    public String productShow(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	ProductInfo productInfo = null;
	if (code != null && code.length() > 0) {
	    productInfo = productDAO.findProductInfo(code);
	}
	if (productInfo == null) {
	    productInfo = new ProductInfo();
	    productInfo.setNewProduct(true);
	}
	model.addAttribute("productForm", productInfo);
	return "product";
    }

    /**
     * This method performs the product deletion from the database
     */
    @RequestMapping(value = { "/productRemove" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String productDelete(Model model, @ModelAttribute("productForm") @Validated ProductInfo productInfo,
	    BindingResult result, final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	if (result.hasErrors()) {
	    return "product";
	}
	try {
	    productDAO.delete(productInfo);
	} catch (HibernateException e) {
	    String message = e.getMessage();
	    model.addAttribute("message", message);
	    return "product";
	}
	return "redirect:/productList";
    }

    /**
     * This method performs the product deletion from the database
     */
    @RequestMapping(value = { "/order" }, method = RequestMethod.GET)
    public String orderView(Model model, @RequestParam("orderId") String orderId) {
	model.addAttribute("loggedinuser", getPrincipal());
	OrderInfo orderInfo = null;
	if (orderId != null) {
	    orderInfo = this.orderDAO.getOrderInfo(orderId);
	}
	if (orderInfo == null) {
	    return "redirect:/orderList";
	}
	List<OrderDetailInfo> details = this.orderDAO.listOrderDetailInfos(orderId);
	orderInfo.setDetails(details);
	model.addAttribute("orderInfo", orderInfo);
	return "order";
    }

    /**
     * This method returns the page with the product list
     */
    @RequestMapping({ "/", "/productList" })
    public String listProductHandler(Model model, @RequestParam(value = "name", defaultValue = "") String likeName,
	    @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "dateDesc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list of the logged in user
     */
    @RequestMapping({ "/userProductList" })
    public String listUserProductHandler(Model model, @RequestParam(value = "name", defaultValue = "") String likeName,
	    @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "default";
	final User currentUser = getLoggedInUser();
	PaginationResult<ProductInfo> carLot = productDAO.queryUserProducts(page, maxResult, maxNavigationPage, filter,
		currentUser);
	model.addAttribute("paginationProducts", carLot);
	return "userProductList";
    }

    /**
     * This method returns the page with the pending product list, where the
     * Administrators can validate all product offers apart from theirs
     */
    @RequestMapping({ "/pendingProductList" })
    public String listProductPendingHandler(Model model,
	    @RequestParam(value = "name", defaultValue = "") String likeName,
	    @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final User currentUser = getLoggedInUser();
	PaginationResult<ProductInfo> carLot = productDAO.queryAdminProducts(page, maxResult, maxNavigationPage,
		currentUser);
	model.addAttribute("paginationProducts", carLot);
	return "pendingProductList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * name in ascending order
     */
    @RequestMapping({ "/productListNameAsc" })
    public String listProductHandlerNameAsc(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "nameAsc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * name in descending order
     */
    @RequestMapping({ "/productListNameDesc" })
    public String listProductHandlerNameDesc(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "nameDesc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * price in ascending order
     */
    @RequestMapping({ "/productListPriceAsc" })
    public String listProductHandlerPriceAsc(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "priceAsc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * price in descending order
     */
    @RequestMapping({ "/productListPriceDesc" })
    public String listProductHandlerPriceDesc(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "priceDesc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * mileage in ascending order
     */
    @RequestMapping({ "/productListMileageAsc" })
    public String listProductHandlerMileageAsc(Model model,
	    @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "mileageAsc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);
	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the product list, filtered by their
     * mileage in descending order
     */
    @RequestMapping({ "/productListMileageDesc" })
    public String listProductHandlerMileageDesc(Model model,
	    @RequestParam(value = "page", defaultValue = "1") int page) {
	model.addAttribute("loggedinuser", getPrincipal());
	final int maxResult = 6;
	final int maxNavigationPage = 10;
	final String filter = "mileageDesc";
	PaginationResult<ProductInfo> carLot = productDAO.queryProducts(page, maxResult, maxNavigationPage, filter);

	model.addAttribute("paginationProducts", carLot);
	return "productList";
    }

    /**
     * This method returns the page with the session shopping cart, after the
     * product status is changed in order not to let anybody else see it in the
     * list anymore until a buying or removal decision is made
     */
    @RequestMapping({ "/buyProduct" })
    public String listProductHandler(HttpServletRequest request, Model model,
	    @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	Product product = null;
	if (code != null && code.length() > 0) {
	    product = productDAO.findProduct(code);
	}
	if (product != null) {
	    CartInfo cartInfo = Utils.getCartInSession(request);
	    ProductInfo productInfo = new ProductInfo(product);
	    cartInfo.addProduct(productInfo, 1);
	    productDAO.update(productInfo, "inCart");
	}
	return "redirect:/shoppingCart";
    }

    /**
     * This method returns the page with the session shopping cart, after the
     * selected product is removed from the cart
     */
    @RequestMapping({ "/shoppingCartRemoveProduct" })
    public String removeProductHandler(HttpServletRequest request, Model model,
	    @RequestParam(value = "code", defaultValue = "") String code) {
	model.addAttribute("loggedinuser", getPrincipal());
	Product product = null;
	if (code != null && code.length() > 0) {
	    product = productDAO.findProduct(code);
	}
	if (product != null) {
	    CartInfo cartInfo = Utils.getCartInSession(request);
	    ProductInfo productInfo = new ProductInfo(product);
	    cartInfo.removeProduct(productInfo);
	    productDAO.update(productInfo, "allow");
	}
	return "redirect:/shoppingCart";
    }

    /**
     * This method displays the shopping cart with the current products list
     */
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
    public String shoppingCartHandler(HttpServletRequest request, Model model) {
	model.addAttribute("loggedinuser", getPrincipal());
	CartInfo myCart = Utils.getCartInSession(request);
	model.addAttribute("cartForm", myCart);
	return "shoppingCart";
    }

    /**
     * This method updates the quantity of a product in the cart
     */
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
    public String shoppingCartUpdateQty(HttpServletRequest request, Model model,
	    @ModelAttribute("cartForm") CartInfo cartForm) {
	model.addAttribute("loggedinuser", getPrincipal());
	CartInfo cartInfo = Utils.getCartInSession(request);
	cartInfo.updateQuantity(cartForm);
	return "redirect:/shoppingCart";
    }

    /**
     * This method provides the environment to enter the customer information
     * for a desired product offer
     */
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
    public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {
	model.addAttribute("loggedinuser", getPrincipal());
	CartInfo cartInfo = Utils.getCartInSession(request);
	User currentUser = getLoggedInUser();
	if (cartInfo.isEmpty()) {
	    return "redirect:/shoppingCart";
	}
	CustomerInfo customerInfo = cartInfo.getCustomerInfo();
	if (customerInfo == null) {
	    customerInfo = new CustomerInfo();
	}
	model.addAttribute("customerForm", customerInfo);
	model.addAttribute("loggedUser", currentUser);
	return "shoppingCartCustomer";
    }

    /**
     * This method sets the customer information in the session shopping cart
     */
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
    public String shoppingCartCustomerSave(HttpServletRequest request, Model model,
	    @ModelAttribute("customerForm") @Validated CustomerInfo customerForm, BindingResult result,
	    final RedirectAttributes redirectAttributes) {
	model.addAttribute("loggedinuser", getPrincipal());
	if (result.hasErrors()) {
	    customerForm.setValid(false);
	    return "shoppingCartCustomer";
	}
	customerForm.setValid(true);
	CartInfo cartInfo = Utils.getCartInSession(request);
	cartInfo.setCustomerInfo(customerForm);
	return "redirect:/shoppingCartConfirmation";
    }

    /**
     * This method returns the page with the shopping cart details confirmation
     */
    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
    public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
	CartInfo cartInfo = Utils.getCartInSession(request);
	model.addAttribute("loggedinuser", getPrincipal());
	if (cartInfo.isEmpty()) {
	    return "redirect:/shoppingCart";
	} else if (!cartInfo.isValidCustomer()) {
	    return "redirect:/shoppingCartCustomer";
	}
	return "shoppingCartConfirmation";
    }

    /**
     * This method performs the redirect for the order payment process page
     */
    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {
	CartInfo cartInfo = Utils.getCartInSession(request);
	model.addAttribute("loggedinuser", getPrincipal());
	if (cartInfo.isEmpty()) {
	    return "redirect:/shoppingCart";
	} else if (!cartInfo.isValidCustomer()) {
	    return "redirect:/shoppingCartCustomer";
	}
	return "redirect:/shoppingCartPayment";
    }

    /**
     * This method provides the environment for the credit card payment process
     */
    @RequestMapping(value = { "/shoppingCartPayment" }, method = RequestMethod.GET)
    public String shoppingCartPayment(HttpServletRequest request, Model model) {
	CartInfo cartInfo = Utils.getCartInSession(request);
	model.addAttribute("loggedinuser", getPrincipal());
	if (cartInfo.isEmpty()) {
	    return "redirect:/shoppingCart";
	} else if (!cartInfo.isValidCustomer()) {
	    return "redirect:/shoppingCartCustomer";
	}
	return "shoppingCartPayment";
    }

    /**
     * This method performs the order payment
     */
    @RequestMapping(value = { "/shoppingCartPayment" }, method = RequestMethod.POST)
    @Transactional(propagation = Propagation.NEVER)
    public String shoppingCartPaymentSave(HttpServletRequest request, Model model) {
	final CartInfo cartInfo = Utils.getCartInSession(request);
	model.addAttribute("loggedinuser", getPrincipal());
	if (cartInfo.isEmpty()) {
	    return "redirect:/shoppingCart";
	} else if (!cartInfo.isValidCustomer()) {
	    return "redirect:/shoppingCartCustomer";
	}
	try {
	    orderDAO.saveOrder(cartInfo);
	    final List<CartLineInfo> soldProduct = cartInfo.getCartLines();
	    for (CartLineInfo currentProduct : soldProduct)
		productDAO.update(currentProduct.getProductInfo(), "sold");
	} catch (HibernateException e) {
	    e.printStackTrace();
	    return "shoppingCartConfirmation";
	}
	Utils.removeCartInSession(request);
	Utils.storeLastOrderFromCartInSession(request, cartInfo);
	return "redirect:/shoppingCartFinalize";
    }

    /**
     * This method returns the page with the order number and successful payment
     * to the user
     */
    @RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
    public String shoppingCartFinalize(HttpServletRequest request, Model model) {
	model.addAttribute("loggedinuser", getPrincipal());
	CartInfo lastOrderCart = Utils.getLastOrderFromCartInSession(request);
	if (lastOrderCart == null) {
	    return "redirect:/shoppingCartConfirmation";
	}
	return "shoppingCartFinalize";
    }

    /**
     * This method incorporates the product image byte data storage in the
     * HTTPServletResponse instance
     */
    @RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
    public void productImage(HttpServletRequest request, HttpServletResponse response, Model model,
	    @RequestParam("code") String code) throws IOException {
	Product product = null;
	if (code != null) {
	    product = this.productDAO.findProduct(code);
	}
	if (product != null && product.getImage() != null) {
	    response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
	    response.getOutputStream().write(product.getImage());
	}
	response.getOutputStream().close();
    }

    /**
     * This method details the product certificate image storage in binary
     * format in the HTTPServletResponse instance
     */
    @RequestMapping(value = { "/productCertificate" }, method = RequestMethod.GET)
    public void productCertificate(HttpServletRequest request, HttpServletResponse response, Model model,
	    @RequestParam("code") String code) throws IOException {
	Product product = null;
	if (code != null) {
	    product = this.productDAO.findProduct(code);
	}
	if (product != null && product.getCertificate() != null) {
	    response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
	    response.getOutputStream().write(product.getCertificate());
	}
	response.getOutputStream().close();
    }

    /**
     * This method details the product identity image storage in binary format
     * in the HTTPServletResponse instance
     */
    @RequestMapping(value = { "/productIdentity" }, method = RequestMethod.GET)
    public void productIdentity(HttpServletRequest request, HttpServletResponse response, Model model,
	    @RequestParam("code") String code) throws IOException {
	Product product = null;
	if (code != null) {
	    product = this.productDAO.findProduct(code);
	}
	if (product != null && product.getIdentity() != null) {
	    response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
	    response.getOutputStream().write(product.getIdentity());
	}
	response.getOutputStream().close();
    }

    /**
     * This method returns the page with the products brands information to the
     * user
     */
    @RequestMapping(value = "/brands", method = RequestMethod.GET)
    public String getBrandsList() {
	return "brands";
    }
}