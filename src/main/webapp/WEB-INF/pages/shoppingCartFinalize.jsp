<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Shopping Cart Finalize</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
	<jsp:include page="header.jsp" />
	� ����
	<div class="total">PAYMENT IN PROGRESS</div>
	���� ����
	<div class="container">
		��������
		<div class="personalInformation">� It seems that your card is
			valid, the payment is now in progress! Once the bank will process the
			transaction, your car will be delivered to the specified address in
			less than 72 hours.</div>

		<div class="container">
			<c:forEach items="${myCart.cartLines}" var="cartLineInfo">
				<div class="product-preview-container">
					<ul>
						<li><img class="product-image"
							src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" /></li>
						<li>Code: ${cartLineInfo.productInfo.code} <input
							type="hidden" name="code"
							value="${cartLineInfo.productInfo.code}" />
						</li>
						<li>Name: ${cartLineInfo.productInfo.name}</li>
						<li>Price: <span class="price"> <fmt:formatNumber
									value="${cartLineInfo.productInfo.price}" type="currency" />
						</span>
						</li>
					</ul>
				</div>
			</c:forEach>
		</div>

		<div class="personalInformation">
			� Your transaction number is: <span style="color: red">${lastOrderedCart.orderNum}</span>
		</div>

		<div class="personalInformation">� Thank you for using our
			management system, we hope this is not the last transaction you
			perform!</div>

		<a class="customWell"
			href="${pageContext.request.contextPath}/productList">TAKE ME
			HOME</a>
	</div>
	� ����<jsp:include page="footer.jsp" />
	�
</body>
</html>