<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.css"
	rel="stylesheet">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<title>German Brands List</title>
</head>
<body>
	<div class="container fill">
		<br>
		<!-- /.header button -->
		<div class="head-btn wow fadeInLeft" style="margin-left: 515px">
			<a href="${pageContext.request.contextPath}/login"
				class="btn btn-danger btn-lg">Return Home</a>
		</div>
		<br>
		<div id="brandsCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#brandsCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#brandsCarousel" data-slide-to="1"></li>
				<li data-target="#brandsCarousel" data-slide-to="2"></li>
				<li data-target="#brandsCarousel" data-slide-to="3"></li>
				<li data-target="#brandsCarousel" data-slide-to="4"></li>
				<li data-target="#brandsCarousel" data-slide-to="5"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">

				<div class="item active">
					<a href="http://www.mercedes-benz.ro/"> <img
						src="images/Mercedes.jpg" alt="Mercedes" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<h3>&emsp;&emsp;Mercedes</h3>
						<p>Mercedes-Benz is a global automobile manufacturer and a
							division of the German company Daimler AG. The brand is known for
							luxury vehicles, buses, coaches, and trucks. The headquarters is
							in Stuttgart, Baden-Württemberg. The name first appeared in 1926
							under Daimler-Benz, but traces its origins to
							Daimler-Motoren-Gesellschaft's 1901 Mercedes and Karl Benz's 1886
							Patent-Motorwagen. The slogan for the brand is "the best or
							nothing" and Mercedes-Benz was one of the top growing brands in
							2014.</p>
						<p>Text source: https://en.wikipedia.org/wiki/Mercedes-Benz
						<p>
					</div>
				</div>

				<div class="item">
					<a href="http://www.porscheromania.ro/ro"> <img
						src="images/Porsche.jpg" alt="Porsche" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<p>Porsche is a German automobile manufacturer specializing in
							high-performance sports cars and SUVs.</p>
						<p>Text source: https://en.wikipedia.org/wiki/Porsche
						<p>
					</div>
				</div>

				<div class="item">
					<a href="http://www.bmw.ro/ro/index.html"> <img
						src="images/BMW.jpg" alt="BMW" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<p>Bayerische Motoren Werke AG, usually known under its
							abbreviation BMW, is a German luxury vehicle, motorcycle, and
							engine manufacturing company founded in 1916. It is one of the
							best-selling luxury automakers in the world. Headquartered in
							Munich, Bavaria, BMW owns Mini cars and is the parent company of
							Rolls-Royce Motor Cars. BMW produces motorcars under the BMW
							Motorsport division and motorcycles under BMW Motorrad, and
							plug-in electric cars under the BMW i sub-brand and the
							"iPerformance" model designation within the regular BMW lineup.</p>
						<p>Text source: https://en.wikipedia.org/wiki/BMW
						<p>
					</div>
				</div>

				<div class="item">
					<a href="https://www.audi.ro/"> <img src="images/Audi.jpg"
						alt="Audi" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<p>Audi is a German automobile manufacturer that designs,
							engineers, produces, markets and distributes luxury vehicles.
							Audi is a member of the Volkswagen Group and has its roots at
							Ingolstadt, Bavaria, Germany. Audi-branded vehicles are produced
							in nine production facilities worldwide. The company name is
							based on the Latin translation of the surname of the founder,
							August Horch. "Horch", meaning "listen" in German, becomes "audi"
							in Latin. The four rings of the Audi logo each represent one of
							four car companies that banded together to create Audi's
							predecessor company, Auto Union. Audi's slogan is Vorsprung durch
							Technik, meaning "Advancement through Technology". However, Audi
							USA had used the slogan "Truth in Engineering" from 2007 to 2016.</p>
						<p>Text source: https://en.wikipedia.org/wiki/Audi
						<p>
					</div>
				</div>

				<div class="item">
					<a href="https://www.volkswagen.ro/"> <img
						src="images/Volkswagen.jpg" alt="Volkswagen" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<p>Volkswagen Group is a German multinational automotive
							manufacturing company headquartered in Wolfsburg, Germany. It
							designs, manufactures and distributes passenger and commercial
							vehicles, motorcycles, engines, and offers related services
							including financing, leasing and fleet management. In 2016, it
							was the largest automaker in the world with sales of 10.3 million
							units, overtaking Toyota.</p>
						<p>Text source: https://en.wikipedia.org/wiki/Volkswagen
						<p>
					</div>
				</div>
				<div class="item">
					<a href="http://www.opel.ro/"> <img src="images/Opel.jpg"
						alt="Opel" style="width: 100%;">
					</a>
					<div class="carousel-caption">
						<p>Opel is a German automobile manufacturer, a subsidiary of
							the General Motors since 1929. In March 2017, the French
							automobile manufacturer Groupe PSA agreed to acquire Opel. The
							acquisition is pending regulatory approvals, including that of
							the European Commission. Opel's headquarters are based in
							Rüsselsheim, Hesse, Germany. The company designs, engineers,
							manufactures, and distributes Opel-branded passenger vehicles,
							light commercial vehicles, and vehicle parts around the world.</p>
						<p>Text source: https://en.wikipedia.org/wiki/Opel
						<p>
					</div>
				</div>

			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#brandsCarousel"
				data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left"></span> <span
				class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#brandsCarousel"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right"></span> <span
				class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
</body>
</html>


