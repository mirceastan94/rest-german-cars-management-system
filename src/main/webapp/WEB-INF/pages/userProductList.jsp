<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>My Car List</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
	<jsp:include page="header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />
	<p class="total">MY CAR LIST</p>

	<c:if test="${empty paginationProducts.list}">
		<p class="noCars">There are no cars available in your list</p>
		<a class="customWell"
			href="${pageContext.request.contextPath}/product">Create a car
			offer</a>
	</c:if>

	<c:forEach items="${paginationProducts.list}" var="prodInfo">

		<div class="product-preview-container">

			<ul>
				<li><img class="product-image"
					src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" /></li>
				<li>VIN: ${prodInfo.code}</li>
				<li>Name: ${prodInfo.name}</li>
				<li>Type: ${prodInfo.type}</li>
				<li>Engine: ${prodInfo.engine}</li>
				<li>Generation: ${prodInfo.generation}</li>
				<li>Year: ${prodInfo.year}</li>
				<li>Mileage: ${prodInfo.mileage}</li>
				<li style="color: green; margin-top: 10px">Information:</li>
				<li>${prodInfo.comments}</li>
				<li style="color: red; font-size: 18px; margin-top: 10px">Price:
					<fmt:formatNumber value="${prodInfo.price}" type="currency" />
				</li>
				<li style="font-size: 16px">Status : <a> ${prodInfo.status}</a></li>
			</ul>

			<div class="btn-group btn-group-justified">
				<c:choose>
					<c:when
						test="${prodInfo.status eq 'REJECTED' || prodInfo.status eq 'PUBLISHED'}">
						<a class="btn btn-primary" style="font-weight: bold; color: black"
							href="${pageContext.request.contextPath}/productEdit?code=${prodInfo.code}">
							EDIT</a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-success" style="font-weight: bold; color: black"
							href="${pageContext.request.contextPath}/productDetail?code=${prodInfo.code}">
							VIEW</a>
					</c:otherwise>
				</c:choose>
			</div>

		</div>
	</c:forEach>

	<br />

	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="userProductList?page=${page}" class="btn btn-danger"
						style="width: 3%; font-weight: bold; margin-top: 10px">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="pagination pagination-lg"> ... </span>
				</c:if>
			</c:forEach>
		</div>
	</c:if>

	<jsp:include page="footer.jsp" />
</body>
</html>