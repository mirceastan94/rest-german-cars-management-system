<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<title>German Cars Management System</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- CSS Files -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.css"
	rel="stylesheet">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" />
<link href="css/css-index-red.css" rel="stylesheet" media="screen">

</head>

<body data-spy="scroll" data-target="#navbar-scroll">

	<div id="preloader"></div>
	<div id="top"></div>

	<div class="fullscreen landing parallax"
		style="background-image: url('images/backGround.jpg');">

		<div class="overlay">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div>
							<a href=""><img src="images/logoSLC.png" alt="logo"></a>
						</div>

						<!-- /.main title -->
						<h1 class="wow fadeInLeft"
							style="margin-left: 10px; margin-top: -5px">German Cars
							Management System</h1>

						<!-- /.header paragraph -->
						<div class="landing-text wow fadeInUp">
							<p>Welcome to the german cars management website. Here today,
								ready for tomorrow!</p>
						</div>

						<!-- /.header button -->
						<div class="head-btn wow fadeInLeft" style="margin-left: 50px">
							<a href="${pageContext.request.contextPath}/brands"
								class="btn-primary">Brands Presentation</a>
						</div>
					</div>
					<!-- /.signup form -->
					<div class="col-md-5">
						<div class="signup-header wow fadeInUp">
							<h3 class="form-title text-center">Credentials Area</h3>
							<c:url var="loginUrl" value="/login" />
							<form action="${loginUrl}" method="post" class="form-horizontal">
								<div class="input-group input-sm">
									<label class="input-group-addon" for="username"><i
										class="fa fa-user"></i></label> <input type="text"
										class="form-control" id="username" name="ssoId"
										placeholder="Enter Username" required>
								</div>
								<div class="input-group input-sm">
									<label class="input-group-addon" for="password"><i
										class="fa fa-lock"></i></label> <input type="password"
										class="form-control" id="password" name="password"
										placeholder="Enter Password" required>
								</div>
								<c:if test="${param.error != null}">
									<div class="col-sm-offset-2">
										<p style="color: #FF0000; font-size: large; font-weight: bold">Invalid
											username or password.</p>
									</div>
								</c:if>
								<c:if test="${param.logout != null}">
									<div class="col-sm-offset-2">
										<p style="color: #82FA58; font-weight: bold;">You have
											been logged out successfully.</p>
									</div>
								</c:if>
								<div class="checkbox"
									style="color: white; transform: scale(1.05)">
									<label><input type="checkbox" id="rememberme"
										name="remember-me"> Remember Me</label>
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />

								<div class="form-actions">
									<input type="submit"
										class="btn btn-block btn-primary btn-default" value="LOG IN">
									<a class="btn btn-block btn-primary btn-default"
										href="<c:url value='/newuser' />">REGISTER</a>
								</div>
								<p class="privacy text-center" style="font-weight: bold;">Your
									credentials will not be shared!</p>
							</form>
						</div>
					</div>
				</div>
				<div style="margin-bottom: 500px"></div>
			</div>
		</div>
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script>
		new WOW().init();
	</script>
</body>
</html>