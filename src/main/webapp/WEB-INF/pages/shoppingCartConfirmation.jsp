<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Shopping Cart Confirmation</title>

<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>
	<jsp:include page="header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />

	<div class="total">ORDER CONFIRMATION</div>

	<div class="customer-info-container">
		<div class="orderConfirmationHeading">Personal details:</div>
		<ul>
			<li>Name: ${myCart.customerInfo.name}</li>
			<li>Email: ${myCart.customerInfo.email}</li>
			<li>Phone: ${myCart.customerInfo.phone}</li>
			<li>Address: ${myCart.customerInfo.address}</li>
			<li>Country: ${myCart.customerInfo.country}</li>
		</ul>
		<div class="orderConfirmationHeading">Cart Summary:</div>
		<ul>
			<li>Cars bought: ${myCart.quantityTotal}</li>
			<li>Total: <span class="subTotal"> <fmt:formatNumber
						value="${myCart.amountTotal}" type="currency" />
			</span></li>
		</ul>
	</div>

	<div class="container">
		<c:forEach items="${myCart.cartLines}" var="cartLineInfo">
			<div class="product-preview-container">
				<ul>
					<li><img class="product-image"
						src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" /></li>
					<li>VIN: ${cartLineInfo.productInfo.code} <input type="hidden"
						name="code" value="${cartLineInfo.productInfo.code}" />
					</li>
					<li>Name: ${cartLineInfo.productInfo.name}</li>
					<li>Price: <span class="price"> <fmt:formatNumber
								value="${cartLineInfo.productInfo.price}" type="currency" />
					</span>
					</li>
				</ul>
			</div>
		</c:forEach>
	</div>

	<form method="POST"
		action="${pageContext.request.contextPath}/shoppingCartConfirmation">
		<a class="customWell" style="margin-left: -25px"
			href="${pageContext.request.contextPath}/shoppingCart">EDIT CART</a>
		<a class="customWell"
			href="${pageContext.request.contextPath}/shoppingCartCustomer">EDIT
			CUSTOMER INFO</a> <input class="customWell" type="submit"
			value="BUY NOW !" class="button-send-sc" />
	</form>

	<jsp:include page="footer.jsp" />

</body>
</html>