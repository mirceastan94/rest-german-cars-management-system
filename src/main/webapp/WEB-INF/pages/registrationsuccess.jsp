<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Confirmation Page</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	<div class="generic-container">

		<div class="alert alert-success lead">${success}</div>

		<span class="well floatRight"> <a
			href="<c:url value='/login' />">LOG IN</a>
		</span>
	</div>
</body>

</html>