<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.css"
	rel="stylesheet">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<title>Review Car</title>
</head>
<body>

	<jsp:include page="header.jsp" />

	<div class="total">SELECTED CAR INFORMATION</div>

	<c:if test="${not empty errorMessage }">
		<div class="error-message">${errorMessage}</div>
	</c:if>

	<form:form modelAttribute="productForm" method="POST"
		enctype="multipart/form-data">
		<div class="carInformation">
			<table class="table-bordered" style="margin: auto">
				<tr>
					<td>VIN</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.code}">
							<form:hidden path="code" />
                        ${productForm.code}
                   </c:if> <c:if test="${empty productForm.code}">
							<form:input path="code" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Name</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.name}">
							<form:hidden path="name" />
                        ${productForm.name}
                   </c:if> <c:if test="${empty productForm.name}">
							<form:input path="name" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Type</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.type}">
							<form:hidden path="type" />
                        ${productForm.type}
                   </c:if> <c:if test="${empty productForm.type}">
							<form:input path="type" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Engine</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.engine}">
							<form:hidden path="engine" />
                        ${productForm.engine}
                   </c:if> <c:if test="${empty productForm.engine}">
							<form:input path="engine" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Generation</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.generation}">
							<form:hidden path="generation" />
                        ${productForm.generation}
                   </c:if> <c:if test="${empty productForm.generation}">
							<form:input path="generation" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Year</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.year}">
							<form:hidden path="year" />
                        ${productForm.year}
                   </c:if> <c:if test="${empty productForm.year}">
							<form:input path="year" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Mileage</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.mileage}">
							<form:hidden path="mileage" />
                        ${productForm.mileage}
                   </c:if> <c:if test="${empty productForm.mileage}">
							<form:input path="mileage" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Price ($)</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.price}">
							<form:hidden path="price" />
                        ${productForm.price}
                   </c:if> <c:if test="${empty productForm.price}">
							<form:input path="price" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

				<tr>
					<td>Comments</td>
					<td style="color: white;"><c:if
							test="${not empty productForm.comments}">
							<form:hidden path="price" />
                        ${productForm.comments}
                   </c:if> <c:if test="${empty productForm.comments}">
							<form:input path="price" />
							<form:hidden path="newProduct" />
						</c:if></td>
				</tr>

			</table>

			<p class="picsDocs">- Pictures and Documents -</p>

			<div id="documentCarousel" class="carousel slide"
				data-ride="carousel" data-interval="60000">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="documentCarousel" data-slide-to="0" class="active"></li>
					<li data-target="documentCarousel" data-slide-to="1"></li>
					<li data-target="documentCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">

					<div class="item active">
						<img class="center-block"
							src="${pageContext.request.contextPath}/productImage?code=${productForm.code}"
							style="width: 600&400;">
					</div>

					<div class="item">
						<img class="center-block"
							src="${pageContext.request.contextPath}/productCertificate?code=${productForm.code}"
							style="width: 400&600;">
					</div>

					<div class="item">
						<img class="center-block"
							src="${pageContext.request.contextPath}/productIdentity?code=${productForm.code}"
							style="width: 400&600;">
					</div>
				</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#documentCarousel"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#documentCarousel"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<a class="customWell"
			href="${pageContext.request.contextPath}/productList">CAR LOT</a>
		<a class="customWell"
			href="${pageContext.request.contextPath}/userProductList">MY CAR
			LIST</a>
	</form:form>
	<jsp:include page="footer.jsp" />
</body>
</html>