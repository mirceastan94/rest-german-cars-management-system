<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cars Statistics</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>

	<jsp:include page="header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />

	<div class="total">STATISTICS</div>

	<div class="statsInfo">
		Total cars bought : <span class="statsValues">${paginationResult.totalRecords}</span>
	</div>
	<div class="statsInfo">
		Budget cars bought : <span class="statsValues">${cheapProductsBought.totalRecords}</span>
	</div>
	<div class="statsInfo">
		Luxury cars bought : <span class="statsValues">${luxuryProductsBought.totalRecords}</span>
	</div>
	<div class="statsInfo">
		Cars bought by romanian people : <span class="statsValues">${romanianProductsBought.totalRecords}</span>
	</div>
	<div class="statsInfo">
		Cars available for purchase : <span class="statsValues">${totalProductsAvailable.totalRecords}</span>
	</div>
	<div class="statsInfo">
		Car offers in review process : <span class="statsValues">${totalProductsPendingAvailable.totalRecords}</span>
	</div>

	<a href="${pageContext.request.contextPath}/dropDownStats"
		class="btn btn-danger btn-md" style="max-width: 200px; margin: 25px">
		<span class="glyphicon glyphicon-stats"></span> Check additional stats
	</a>

	<div class="total">ORDER LIST</div>

	<table class="table table-bordered">

		<tr>
			<th class="tableInfo">#</th>
			<th class="tableInfo">Order Date</th>
			<th class="tableInfo">Customer Name</th>
			<security:authorize access="hasRole('ADMIN')">
				<th class="tableInfo">Customer Address</th>
				<th class="tableInfo">Customer Email</th>
			</security:authorize>
			<th class="tableInfo">Amount</th>
			<security:authorize access="hasRole('ADMIN')">
				<th class="tableInfo">View</th>
			</security:authorize>
		</tr>

		<c:forEach items="${paginationResult.list}" var="orderInfo">
			<tr class="tableInfo">
				<th class="tableInfo" scope="row">${orderInfo.orderNum}</th>
				<th class="tableInfo"><fmt:formatDate
						value="${orderInfo.orderDate}" pattern="dd-MM-yyyy HH:mm" /></th>
				<th class="tableInfo">${orderInfo.customerName}</th>
				<security:authorize access="hasRole('ADMIN')">
					<th class="tableInfo">${orderInfo.customerAddress}</th>
					<th class="tableInfo">${orderInfo.customerEmail}</th>
				</security:authorize>
				<th class="tableInfo" style="color: red;"><fmt:formatNumber
						value="${orderInfo.amount}" type="currency" /></th>
				<security:authorize access="hasRole('ADMIN')">
					<th class="tableInfo"><a
						href="${pageContext.request.contextPath}/order?orderId=${orderInfo.id}">
							View</a></th>
				</security:authorize>
			</tr>
		</c:forEach>

	</table>

	<c:if test="${paginationResult.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationResult.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="statistics?page=${page}" class="btn btn-danger"
						style="width: 3%; font-weight: bold; margin-top: 10px">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="nav-item"> ... </span>
				</c:if>
			</c:forEach>
		</div>
	</c:if>

	<jsp:include page="footer.jsp" />
</body>
</html>