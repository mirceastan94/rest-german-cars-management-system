<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Order Information</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
	<jsp:include page="header.jsp" />
	<fmt:setLocale value="en_US" scope="session" />

	<div class="total">ORDER DETAILS</div>

	<div class="customer-info-container">
		<div class="orderConfirmationHeading">Personal details:</div>
		<ul>
			<li>Name: ${orderInfo.customerName}</li>
			<li>Email: ${orderInfo.customerEmail}</li>
			<li>Phone: ${orderInfo.customerPhone}</li>
			<li>Address: ${orderInfo.customerAddress}</li>
			<li>Country: ${orderInfo.customerCountry}</li>
		</ul>
	</div>

	<table class="table table-bordered" style="border: 3px solid #ccc;">
		<tr>
			<th class="tableInfo">Car code</th>
			<th class="tableInfo">Car name</th>
			<th class="tableInfo">Car type</th>
			<th class="tableInfo">Car mileage</th>
			<th class="tableInfo">Car price</th>
		</tr>
		<c:forEach items="${orderInfo.details}" var="orderDetailInfo">
			<tr>
				<td class="tableInfo">${orderDetailInfo.productCode}</td>
				<td class="tableInfo">${orderDetailInfo.productName}</td>
				<td class="tableInfo">${orderDetailInfo.type}</td>
				<td class="tableInfo">${orderDetailInfo.mileage}</td>
				<td class="tableInfo" style="color: red"><fmt:formatNumber
						value="${orderDetailInfo.price}" type="currency" /></td>
			</tr>
		</c:forEach>
	</table>

	<jsp:include page="footer.jsp" />
</body>
</html>