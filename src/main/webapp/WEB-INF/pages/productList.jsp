<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Car List</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
	<jsp:include page="header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />
	<p class="total">PUBLISHED CAR LOT</p>

	<c:if test="${empty paginationProducts.list}">
		<p class="noCars">There are no cars available to buy !</p>
		<a class="customWell"
			href="${pageContext.request.contextPath}/product">Create a car
			offer</a>
	</c:if>

	<c:forEach items="${paginationProducts.list}" var="prodInfo">
		<div class="product-preview-container">
			<ul>
				<li><img class="product-image"
					src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" /></li>
				<li>Chassis: ${prodInfo.code}</li>
				<li>Name: ${prodInfo.name}</li>
				<li>Type: ${prodInfo.type}</li>
				<li>Engine: ${prodInfo.engine}</li>
				<li>Generation: ${prodInfo.generation}</li>
				<li>Year: ${prodInfo.year}</li>
				<li>Mileage: ${prodInfo.mileage}</li>
				<li style="color: green; margin-top: 10px">Information:</li>
				<li>${prodInfo.comments}</li>
				<li style="color: red; font-size: 18px; margin-top: 10px">Price:
					<fmt:formatNumber value="${prodInfo.price}" type="currency" />
				</li>
			</ul>
			<div class="btn-group btn-group-justified">
				<a class="btn btn-info" style="font-weight: bold; color: black"
					href="${pageContext.request.contextPath}/productDetail?code=${prodInfo.code}">
					VIEW</a>
				<security:authorize access="hasRole('ADMIN') or hasRole('USER')"
					var="registered" />
				<c:choose>
					<c:when test="${registered}">
						<a class="btn btn-success" style="font-weight: bold; color: black"
							href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}">
							BUY</a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-success disabled" title="Log In to BUY"
							style="font-weight: bold; color: black;"
							href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}">
							BUY</a>
					</c:otherwise>
				</c:choose>
				<security:authorize access="hasRole('ADMIN')">
					<a class="btn btn-warning" style="font-weight: bold; color: black"
						href="${pageContext.request.contextPath}/productRemove?code=${prodInfo.code}">
						DELETE</a>
				</security:authorize>
			</div>
		</div>
	</c:forEach>
	<br />
	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="productList?page=${page}" class="btn btn-danger"
						style="width: 3%; font-weight: bold; margin-top: 10px">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="pagination pagination-lg"> ... </span>
				</c:if>
			</c:forEach>
		</div>
	</c:if>
	<jsp:include page="footer.jsp" />
</body>
</html>