<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Customer Information</title>

<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="js/datetime.js"></script>
</head>
<body>
	<jsp:include page="header.jsp" />

	<div class="personalInformation">Enter your personal information
		below ...</div>

	<form:form method="POST" id="custForm" modelAttribute="customerForm"
		action="${pageContext.request.contextPath}/shoppingCartCustomer">

		<div class="form">
			<table>
				<tr>
					<td>Name *</td>
					<td><form:input path="name"
							value="${loggedUser.firstName} ${loggedUser.lastName}" /></td>
					<td><form:errors path="name" class="error-message" /></td>
				</tr>

				<tr>
					<td>Email *</td>
					<td><form:input path="email" value="${loggedUser.email}" /></td>
					<td><form:errors path="email" class="error-message" /></td>
				</tr>

				<tr>
					<td>Phone *</td>
					<td><form:input path="phone" type="number"
							placeholder="Enter your phone number" /></td>
					<td><form:errors path="phone" class="error-message" /></td>
				</tr>

				<tr>
					<td>Address *</td>
					<td><form:input path="address"
							placeholder="Enter your address" /></td>
					<td><form:errors path="address" class="error-message" /></td>
				</tr>

				<tr>
					<td>Country *</td>
					<td><form:input path="country" value="${loggedUser.country}" /></td>
					<td><form:errors path="country" class="error-message" /></td>
				</tr>
			</table>
		</div>
		<input class="customWell" type="SUBMIT" value="SUBMIT" />

	</form:form>


	<jsp:include page="footer.jsp" />


</body>
</html>