<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Shopping Cart Finalize</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body>
	    <jsp:include page="header.jsp" />
	      
	<div class="total">ORDER PAYMENT</div>
	         
	<div class="container">
		<div
			class="panel panel-default credit-card-box customwidth center-block"
			style="max-width: 600px;">
			<div class="panel-heading display-table">
				<div class="row display-tr">
					<div class="display-td">
						<img style="transform: scale(1.3)" src="images/cardList.png">
					</div>
				</div>
			</div>
			<div class="panel-body">
				<form method="POST"
					action="${pageContext.request.contextPath}/shoppingCartPayment">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="cardNumber" class="cardLabels">CARD NUMBER</label>
								<div class="input-group">
									<input type="tel" class="form-control" name="cardNumber"
										placeholder="Valid Card Number" autocomplete="off"
										pattern=".{15,}" required autofocus /> <span
										class="input-group-addon"><i class="fa fa-credit-card"
										id="cardlogo" style="color: purple; font-size: 10px;"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-7 col-md-7">
							<div class="form-group">
								<label for="cardExpiry"><span class="cardLabels">EXPIRATION
										DATE</span></label> <input type="tel" class="form-control" name="cardExpiry"
									placeholder="MM / YY" autocomplete="off" required />
							</div>
						</div>

						<div class="col-xs-5 col-md-5 pull-right">
							<div class="form-group">
								<label for="cardCVC" class="cardLabels">CVC</label> <input
									type="tel" class="form-control" name="cardCVC"
									placeholder="CVC" autocomplete="off" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button class="customWell" type="submit">PERFORM PAYMENT</button>
						</div>
					</div>
					<div class="row" style="display: none;">
						<div class="col-xs-12">
							<p class="payment-errors"></p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	      <jsp:include page="footer.jsp" />
	 
</body>
</html>