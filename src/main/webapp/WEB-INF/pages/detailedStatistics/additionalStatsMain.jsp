<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Additional Car Statistics</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>

	<jsp:include page="../header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />

	<div class="total">ADDITIONAL CAR STATISTICS</div>

	<jsp:include page="statisticsButtons.jsp" />

</body>
</html>