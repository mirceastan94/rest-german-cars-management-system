<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Additional Car Statistics</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>

	<jsp:include page="additionalStatsMain.jsp" />

	<table class="table table-bordered">

		<tr>
			<th class="tableInfo tableTop">Period</th>
			<th class="tableInfo tableTop">Cars sold</th>
		</tr>

		<c:forEach items="${soldProductsPeriod}" var="soldCarsPeriod">
			<tr class="tableInfo">
				<th class="tableInfo" scope="row">${soldCarsPeriod.key}</th>
				<th class="tableInfo">${soldCarsPeriod.value}</th>
			</tr>
		</c:forEach>

	</table>

	<jsp:include page="../footer.jsp" />


</body>
</html>