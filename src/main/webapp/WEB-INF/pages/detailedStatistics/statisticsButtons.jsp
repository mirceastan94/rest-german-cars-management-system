<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" type="text/css" href="css/styles.css">

<div class="btn-group btn-group-lg" style="margin-left: 10px">
	<a class="btn btn-danger statsButtons"
		href="<c:url value='/soldProductsPeriod' />">Sold cars by period</a> <a
		class="btn btn-success statsButtons"
		href="<c:url value='/soldProductsBrand' />">Sold cars by brand</a> <a
		class="btn btn-info statsButtons"
		href="<c:url value='/soldProductsPrice' />">Sold cars by price</a> <a
		class="btn btn-primary statsButtons"
		href="<c:url value='/soldProductsType' />">Sold cars by type</a> <a
		class="btn btn-warning statsButtons"
		href="<c:url value='/soldProductsMileage' />">Sold cars by mileage</a>
</div>