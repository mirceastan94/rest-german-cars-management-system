<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Car Shopping Cart</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>
	<jsp:include page="header.jsp" />

	<fmt:setLocale value="en_US" scope="session" />

	<div class="total">SHOPPING CART</div>

	<c:if test="${empty cartForm or empty cartForm.cartLines}">
		<p class="noCars">There are no cars in your cart !</p>
		<a class="customWell"
			href="${pageContext.request.contextPath}/productList">Check our
			available car lot</a>
	</c:if>

	<c:if test="${not empty cartForm and not empty cartForm.cartLines   }">
		<form:form method="POST" modelAttribute="cartForm"
			action="${pageContext.request.contextPath}/shoppingCart">

			<c:forEach items="${cartForm.cartLines}" var="cartLineInfo"
				varStatus="varStatus">
				<div class="product-preview-container">
					<ul>
						<li><img class="product-image"
							src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" />
						</li>
						<li>VIN: ${cartLineInfo.productInfo.code} <form:hidden
								path="cartLines[${varStatus.index}].productInfo.code" />
						</li>
						<li>Name: ${cartLineInfo.productInfo.name}</li>
						<li>Price: <span class="price"> <fmt:formatNumber
									value="${cartLineInfo.productInfo.price}" type="currency" />
						</span></li>
						<li><a class="btn btn-danger"
							style="margin-top: 15px; margin-bottom: 10px; margin-left: 75px; min-width: 150px; max-width: 280px"
							href="${pageContext.request.contextPath}/shoppingCartRemoveProduct?code=${cartLineInfo.productInfo.code}">
								REMOVE</a></li>
					</ul>
				</div>
			</c:forEach>
			<div>
				<a class="customWell"
					href="${pageContext.request.contextPath}/shoppingCartCustomer">ENTER
					CUSTOMER INFO</a><a class="customWell"
					href="${pageContext.request.contextPath}/productList">BUY
					ANOTHER CARS</a>
			</div>
		</form:form>

	</c:if>
	<jsp:include page="footer.jsp" />

</body>
</html>