<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<script type="text/javascript" src="js/datetime.js"></script>

<div class="header-container">
	<div class="site-name">
		German Cars Management System <img src="images/logoSLC.png" alt="logo">
	</div>
	<span id="date_time" class="time"></span>
	<script type="text/javascript">
		window.onload = date_time('date_time');
	</script>

	<a href="${pageContext.request.contextPath}/productList"
		class="btn btn-danger btn-md"
		style="max-width: 95px; margin-right: 100px"> <span
		class="glyphicon glyphicon-home"></span> Home
	</a>

	<div class="user">
		<span> Welcome - <strong>${loggedinuser}</strong>
		</span>
		<p class="glyphicon glyphicon-user"></p>
	</div>
</div>

<div class="container" style="margin-top: 20px">
	<div class="row">
		<nav class="navbar navbar-default navbar-inverse navbar-static-top"
			role="navigation">
			<ul class="nav navbar-nav pull-right">
				<li><a
					href="${pageContext.request.contextPath}/userProductList">My
						car list</a></li>
				<li><a href="${pageContext.request.contextPath}/shoppingCart">Shopping
						cart</a></li>
				<li><a href="${pageContext.request.contextPath}/statistics">Statistics</a></li>
				<li><a href="${pageContext.request.contextPath}/product">New
						car offer</a></li>
				<security:authorize access="hasRole('ADMIN')">
					<li><a
						href="${pageContext.request.contextPath}/pendingProductList">Validate
							cars</a></li>
					<li><a href="${pageContext.request.contextPath}/list">Admin
							panel</a></li>
				</security:authorize>
				<li class="dropdown show-on-hover"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Filter cars <span class="caret"></span>
				</a>
					<ul class="dropdown-menu" style="background-color: #171717">
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListNameAsc">by
								Name (A-Z)</a></li>
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListNameDesc">by
								Name (Z-A)</a></li>
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListPriceAsc">by
								Price (1-N) $</a></li>
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListPriceDesc">by
								Price (N-1) $</a></li>
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListMileageAsc">by
								Mileage (0-N) KM</a></li>
						<li style="margin-left: 25px"><a
							style="font-weight: bold; color: red;"
							href="${pageContext.request.contextPath}/productListMileageDesc">by
								Mileage (N-0) KM</a></li>
					</ul></li>
				<c:choose>
					<c:when test="${loggedinuser == 'anonymousUser' }">
						<li><a href="<c:url value="/login" />">Login</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="<c:url value="/logout" />">Logout</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>
	</div>
</div>

