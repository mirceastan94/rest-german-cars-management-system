<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Car Information</title>

<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body>

	<jsp:include page="header.jsp" />

	<div class="total">CAR INFORMATION</div>

	<c:if test="${not empty errorMessage }">
		<div class="error-message">${errorMessage}</div>
	</c:if>

	<form:form modelAttribute="productForm" method="POST"
		enctype="multipart/form-data">
		<div class="carInformation">
			<table>

				<tr>
					<td>Chassis number</td>
					<td style="color: red;"><c:if
							test="${not empty productForm.code}">
							<form:input path="code" />
							<form:hidden path="newProduct" />
                   </c:if> <c:if test="${empty productForm.code}">
							<form:input path="code" />
							<form:hidden path="newProduct" />
						</c:if></td>
					<td><form:errors path="code" class="error-message" /></td>
				</tr>

				<tr>
					<td>Name</td>
					<td><form:input path="name" /></td>
					<td><form:errors path="name" class="error-message" /></td>
				</tr>

				<tr>
					<td>Type</td>
					<td><form:select path="type"
							style="min-width: 280px; text-align-last:center">
							<form:option value="Cabrio">Cabriolet</form:option>
							<form:option value="Sedan">Sedan</form:option>
							<form:option value="Coupe">Coupe</form:option>
							<form:option value="Hatchback">Hatchback</form:option>
							<form:option value="Break">Break</form:option>
							<form:option value="SUV">SUV</form:option>
						</form:select></td>
					<td><form:errors path="type" class="error-message" /></td>
				</tr>

				<tr>
					<td>Engine</td>
					<td><form:input path="engine" type="number" step="0.01" /></td>
					<td><form:errors path="engine" class="error-message" /></td>
				</tr>

				<tr>
					<td>Generation</td>
					<td><form:input path="generation" /></td>
					<td><form:errors path="generation" class="error-message" /></td>
				</tr>

				<tr>
					<td>Year</td>
					<td><form:input path="year" type="number" /></td>
					<td><form:errors path="year" class="error-message" /></td>
				</tr>

				<tr>
					<td>Mileage</td>
					<td><form:input path="mileage" type="number" /></td>
					<td><form:errors path="mileage" class="error-message" /></td>
				</tr>

				<tr>
					<td>Price</td>
					<td><form:input path="price" type="number" /></td>
					<td><form:errors path="price" class="error-message" /></td>
				</tr>

				<tr>
					<td>Description</td>
					<td><form:textarea
							placeholder="Please enter your car description here."
							path="comments" rows="5" cols="35" /></td>
					<td><form:errors path="comments" class="error-message" /></td>
				</tr>

				<tr>
					<td>Car image</td>
					<td style="padding-left: 50px"><form:input type="file"
							path="fileDataImage" /></td>
					<td><img
						src="${pageContext.request.contextPath}/productImage?code=${productForm.code}"
						width="400&300" /></td>
					<td><form:errors path="fileDataImage" class="error-message" /></td>
				</tr>

				<tr>
					<td>Certificate of car title</td>
					<td style="padding-left: 50px"><form:input type="file"
							path="fileCertificateDocument" /></td>
					<td><img
						src="${pageContext.request.contextPath}/productCertificate?code=${productForm.code}"
						width="400&300" /></td>
					<td><form:errors path="fileCertificateDocument"
							class="error-message" /></td>
				</tr>

				<tr>
					<td>Vehicle identity card</td>
					<td style="padding-left: 50px"><form:input type="file"
							path="fileIdentityDocument" /></td>
					<td><img
						src="${pageContext.request.contextPath}/productIdentity?code=${productForm.code}"
						width="400&300" /></td>
					<td><form:errors path="fileIdentityDocument"
							class="error-message" /></td>
				</tr>
			</table>
		</div>

		<div class="form" style="color: red; margin-left: -1000px">All
			fields are mandatory!</div>
		<input class="customWell" type="submit" value="SUBMIT" />
		<input class="customWell" type="reset" value="RESET" />

	</form:form>
	<jsp:include page="footer.jsp" />

</body>
</html>